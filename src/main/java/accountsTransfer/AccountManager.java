package accountsTransfer;

import java.util.ArrayList;
import java.util.List;

public class AccountManager {

    private static List<Account> accounts;

    public AccountManager() {
        accounts= new ArrayList<>();
    }

    public void addAccount(Account account) {
        accounts.add(account);
    }

    public static Account findByAccountNumber(String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;
    }

}
