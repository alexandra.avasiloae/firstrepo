package accountsTransfer;


public class Account {

    private Double balance;
    private String accountNumber;
    private String ownerName;

    public Account(Double balance, String accountNumber, String ownerName) {
        this.balance = balance;
        this.accountNumber = accountNumber;
        this.ownerName = ownerName;
    }

    public Account() {
        this.accountNumber = null;
        this.ownerName = null;
        this.balance = null;
    }

    public boolean checkAccountNumberForTransfer(String accountNumber) {
        return accountNumber.length() == 26;
    }

    public boolean checkAllFieldForTransfer(String accountNumber, String ownerName, Double amount) {
        return accountNumber != null && amount != null && ownerName != null;
    }

    public String transfer(String accountNumber, String ownerName, Double amount) {
        if (checkAllFieldForTransfer(accountNumber, ownerName, amount)) {
            if (checkAccountNumberForTransfer(accountNumber)) {
                Account account = AccountManager.findByAccountNumber(accountNumber);
                if (account != null) {
                    if (account.balance > amount) {
                        account.balance += amount;
                        this.balance -= amount;
                    } else {
                        return "Insufficient funds";
                    }
                } else {
                    return "This account number does not exist!";
                }
            } else {
                return "Account number is not correct!";
            }
        } else {
            return "All fields has not been completed!";
        }
        return "Success";
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }
}
