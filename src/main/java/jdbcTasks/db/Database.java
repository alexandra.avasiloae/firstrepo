package jdbcTasks.db;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Database {

    private Connection con = null;
    private Statement stmt;
    private static Database single_instance = null;

    private Database() {
        try {
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/credit_agency", "root", "my-secret-pw");
            con.setAutoCommit(false);
            stmt = con.createStatement();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Connection getCon() {
        return con;
    }

    public static Database getInstance() {
        if (single_instance == null)
            single_instance = new Database();
        return single_instance;
    }

    public void disconnect() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public void executeDMLCommand(String command) {
        try {
            stmt.execute(command);
            commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ResultSet executeQueries(String sql) {
        ResultSet resultSet = null;
        try {
            resultSet = stmt.executeQuery(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultSet;
    }

    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
