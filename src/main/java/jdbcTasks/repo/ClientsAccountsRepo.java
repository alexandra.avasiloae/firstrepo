package jdbcTasks.repo;

import jdbcTasks.db.Database;
import modelForCraditAgencyDb.Account;
import modelForCraditAgencyDb.Client;
import modelForCraditAgencyDb.ClientsAccounts;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClientsAccountsRepo extends FactoryMethod<ClientsAccounts> implements Repo<ClientsAccounts> {
    private final Database db;
    private final ClientsRepo clientsRepo = new ClientsRepo();
    private final AccountRepo accountRepo = new AccountRepo();
    private final List<Client> clients = getAllClients();
    private final List<Account> accounts = getAllAccounts();

    public List<Client> getAllClients() {
        return clientsRepo.listAll();
    }

    public Client findClient(int id) {
        for (Client client : clients) {
            if (client.getClientId().equals(id)) {
                return client;
            }
        }
        return null;
    }

    public List<Account> getAllAccounts() {
        return accountRepo.listAll();
    }

    public Account findAccount(int id) {
        for (Account account : accounts) {
            if (account.getAccountId().equals(id)) {
                return account;
            }
        }
        return null;
    }

    public ClientsAccountsRepo() {
        db = Database.getInstance();
    }

    @Override
    public ClientsAccounts readById(int id) {
        String sql = " SELECT * FROM clients_accounts WHERE id = " + id;
        return createListOfObjectsByQuery(sql).get(0);
    }

    @Override
    public List<ClientsAccounts> listAll() {
        String sql = " SELECT * FROM clients_accounts";
        return createListOfObjectsByQuery(sql);
    }

    @Override
    public void insert(ClientsAccounts clientsAccounts) {
        String command = "INSERT INTO credit_agency.clients_accounts (client_id, account_id) VALUES (?, ?)";
        try {
            PreparedStatement preparedStatement = db.getCon().prepareStatement(command, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, clientsAccounts.getClient().getClientId());
            preparedStatement.setInt(2, clientsAccounts.getAccount().getAccountId());
            preparedStatement.executeUpdate();
            db.commit();
            ResultSet keyResultSet = preparedStatement.getGeneratedKeys();
            if (keyResultSet.next()) {
                int newId = keyResultSet.getInt(1);
                clientsAccounts.setId(newId);
            }
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(ClientsAccounts clientsAccounts) {

    }

    @Override
    public void delete(ClientsAccounts clientsAccounts) {
        String command = "delete from credit_agency.clients_accounts WHERE id = " + clientsAccounts.getId();
        db.executeDMLCommand(command);
    }

    @Override
    List<ClientsAccounts> createListOfObjectsByQuery(String sql) {
        List<ClientsAccounts> clientsAccountsList = new ArrayList<>();
        try {
            ResultSet rs = db.executeQueries(sql);
            while (rs.next()) {
                ClientsAccounts clientsAccounts = new ClientsAccounts();
                Account account = findAccount(rs.getInt("id_account"));
                Client client = findClient(rs.getInt("id_client"));
                clientsAccounts.setAccount(account);
                clientsAccounts.setId(rs.getInt("id"));
                clientsAccounts.setClient(client);
                clientsAccountsList.add(clientsAccounts);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientsAccountsList;
    }
}

