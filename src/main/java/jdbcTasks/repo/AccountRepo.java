package jdbcTasks.repo;

import jdbcTasks.db.Database;
import modelForCraditAgencyDb.Account;
import modelForCraditAgencyDb.CreditOffers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class AccountRepo extends FactoryMethod<Account> implements Repo<Account> {

    private final Database db;
    private final CreditOffersRepo creditOffersRepo = new CreditOffersRepo();
    private final List<CreditOffers> creditOffersList = getAllCreditOffers();

    public List<CreditOffers> getAllCreditOffers() {
        return creditOffersRepo.listAll();
    }

    public CreditOffers findCreditOffer(int id) {
        for (CreditOffers creditOffers : creditOffersList) {
            if (creditOffers.getCreditId().equals(id)) {
                return creditOffers;
            }
        }
        return null;
    }

    public AccountRepo() {
        db = Database.getInstance();
    }

    @Override
    public Account readById(int id) {
        String sql = " SELECT * FROM accounts WHERE account_id = " + id;
        return createListOfObjectsByQuery(sql).get(0);
    }

    @Override
    public List<Account> listAll() {
        String sql = " SELECT * FROM accounts";
        return createListOfObjectsByQuery(sql);
    }

    @Override
    public void insert(Account account) {
        String command = "INSERT INTO credit_agency.accounts (account_number, credit_offer_id) VALUES (?, ?)";
        try {
            PreparedStatement preparedStatement = db.getCon().prepareStatement(command, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, account.getAccountNumber());
            preparedStatement.setInt(2, account.getCreditOffer().getCreditId());
            preparedStatement.executeUpdate();
            db.commit();
            ResultSet keyResultSet = preparedStatement.getGeneratedKeys();
            if (keyResultSet.next()) {
                int newAccountId = keyResultSet.getInt(1);
                account.setAccountId(newAccountId);
                account.setBalance(0D);
            }
            preparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Account account) {
        String command = "UPDATE credit_agency.accounts set balance = "
                + "'" + account.getBalance() + "' WHERE account_id = " + account.getAccountId();
        db.executeDMLCommand(command);
    }

    @Override
    public void delete(Account account) {
        String command = "delete from credit_agency.accounts WHERE account_id = " + account.getAccountId();
        db.executeDMLCommand(command);
    }

    @Override
    List<Account> createListOfObjectsByQuery(String sql) {
        List<Account> accountList = new ArrayList<>();
        try {
            ResultSet rs = db.executeQueries(sql);
            while (rs.next()) {
                Account account = new Account();
                account.setAccountId(rs.getInt("account_id"));
                account.setAccountNumber(rs.getString("account_number"));
                account.setBalance(rs.getDouble("balance"));
                CreditOffers creditOffers = findCreditOffer(rs.getInt("credit_offer_id"));
                account.setCreditOffer(creditOffers);
                accountList.add(account);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountList;
    }
}
