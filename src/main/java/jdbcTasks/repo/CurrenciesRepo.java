package jdbcTasks.repo;

import jdbcTasks.db.Database;
import modelForCraditAgencyDb.Account;
import modelForCraditAgencyDb.Currencies;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class CurrenciesRepo extends FactoryMethod<Currencies> implements Repo<Currencies> {

    private final Database db;
    private final AccountRepo accountRepo = new AccountRepo();
    private final List<Account> accounts = getAllAccounts();

    public List<Account> getAllAccounts() {
        return accountRepo.listAll();
    }

    public Account findAccount(int id) {
        for (Account account : accounts) {
            if (account.getAccountId().equals(id)) {
                return account;
            }
        }
        return null;
    }

    public CurrenciesRepo() {
        db = Database.getInstance();
    }

    @Override
    public Currencies readById(int id) {
        String sql = " SELECT * FROM currencies WHERE account_id = " + id;
        return createListOfObjectsByQuery(sql).get(0);
    }

    @Override
    public List<Currencies> listAll() {
        String sql = " SELECT * FROM currencies";
        return createListOfObjectsByQuery(sql);
    }

    @Override
    public void insert(Currencies currencies) {
        String command = "INSERT INTO credit_agency.currencies (account_id, currencie, country) VALUES ("
                + currencies.getAccount().getAccountId() + " , '" + currencies.getCurrencie() + "' , '" + currencies.getCountry() + "')";
        db.executeDMLCommand(command);
    }

    @Override
    public void update(Currencies currencies) {
        String command = "UPDATE credit_agency.currencies set currencie = "
                + "'" + currencies.getCurrencie() + "' WHERE account_id = " + currencies.getAccount().getAccountId();
        db.executeDMLCommand(command);
    }

    @Override
    public void delete(Currencies currencies) {
        String command = "delete from credit_agency.currencies WHERE account_id = " + currencies.getAccount().getAccountId();
        db.executeDMLCommand(command);
    }

    @Override
    List<Currencies> createListOfObjectsByQuery(String sql) {
        List<Currencies> currenciesList = new ArrayList<>();
        try {
            ResultSet rs = db.executeQueries(sql);
            while (rs.next()) {
                Currencies currencies = new Currencies();
                Account account = findAccount(rs.getInt("account_id"));
                currencies.setAccount(account);
                currencies.setCurrencie(rs.getString("currencie"));
                currencies.setCountry(rs.getString("country"));
                currenciesList.add(currencies);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currenciesList;
    }
}
