package jdbcTasks.repo;

import jdbcTasks.db.Database;
import modelForCraditAgencyDb.ClientsAccounts;
import modelForCraditAgencyDb.Transaction;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class TransactionRepo extends FactoryMethod<Transaction> implements Repo<Transaction> {

    private final Database db;
    private final ClientsAccountsRepo clientsAccountsRepo = new ClientsAccountsRepo();
    private final List<ClientsAccounts> clientsAccountsList = getAllClientsAccounts();

    public ClientsAccounts findClientsAccounts(int id) {
        for (ClientsAccounts clientsAccounts : clientsAccountsList) {
            if (clientsAccounts.getId().equals(id)) {
                return clientsAccounts;
            }
        }
        return null;
    }

    public List<ClientsAccounts> getAllClientsAccounts() {
        return clientsAccountsRepo.listAll();
    }

    public TransactionRepo() {
        db = Database.getInstance();
    }

    @Override
    public Transaction readById(int id) {
        String sql = " SELECT * FROM transactions WHERE transaction_id = " + id;
        return createListOfObjectsByQuery(sql).get(0);
    }

    @Override
    public List<Transaction> listAll() {
        String sql = " SELECT * FROM transactions";
        return createListOfObjectsByQuery(sql);
    }

    @Override
    public void insert(Transaction transaction) {
        String command = "INSERT INTO credit_agency.transactions (transaction_id, transaction_type, amount) VALUES ("
                + transaction.getClientsAccounts().getId() + ", '" + transaction.getTransactionType() + "' , " + transaction.getAmount() + ")";
        db.executeDMLCommand(command);
    }

    @Override
    public void update(Transaction transaction) {
        String command = "UPDATE credit_agency.transactions set amount = "
                + transaction.getAmount() + " WHERE credit_id = " + transaction.getClientsAccounts().getId();
        db.executeDMLCommand(command);
    }

    @Override
    public void delete(Transaction transaction) {
        String command = "delete from credit_agency.transactions WHERE transaction_id = " + transaction.getClientsAccounts().getId();
        db.executeDMLCommand(command);
    }

    @Override
    List<Transaction> createListOfObjectsByQuery(String sql) {
        List<Transaction> transactionList = new ArrayList<>();
        try {
            ResultSet rs = db.executeQueries(sql);
            while (rs.next()) {
                Transaction transaction = new Transaction();
                transaction.setTransactionType(rs.getString("transaction_type"));
                transaction.setAmount(rs.getDouble("amount"));
                ClientsAccounts clientsAccounts = findClientsAccounts(rs.getInt("transaction_id"));
                transaction.setClientsAccounts(clientsAccounts);
                transactionList.add(transaction);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transactionList;
    }
}
