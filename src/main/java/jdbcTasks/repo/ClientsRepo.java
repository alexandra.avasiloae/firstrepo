package jdbcTasks.repo;

import jdbcTasks.db.Database;
import modelForCraditAgencyDb.Client;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ClientsRepo extends FactoryMethod<Client> implements Repo<Client> {

    private final Database db;

    public ClientsRepo() {
        db = Database.getInstance();
    }

    @Override
    public Client readById(int id) {
        String sql = " SELECT * FROM clients WHERE client_id = " + id;
        return createListOfObjectsByQuery(sql).get(0);
    }

    @Override
    public List<Client> listAll() {
        String sql = " SELECT * FROM clients";
        return createListOfObjectsByQuery(sql);
    }

    @Override
    public void insert(Client client) {
        String command = "INSERT INTO credit_agency.clients (client_id, name) VALUES (null, ?)";
        try {
            PreparedStatement preparedStatement = db.getCon().prepareStatement(command, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, client.getName());
            preparedStatement.executeUpdate();
            db.commit();
            ResultSet keyResultSet = preparedStatement.getGeneratedKeys();
            if (keyResultSet.next()) {
                int newClientId = keyResultSet.getInt(1);
                client.setClientId(newClientId);
            }
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(Client client) {
        String command = "UPDATE credit_agency.clients set name = "
                + "'" + client.getName() + "' WHERE client_id = " + client.getClientId();
        db.executeDMLCommand(command);
    }

    @Override
    public void delete(Client client) {
        String command = "delete from credit_agency.clients WHERE client_id = " + client.getClientId();
        db.executeDMLCommand(command);
    }

    @Override
    List<Client> createListOfObjectsByQuery(String sql) {
        List<Client> clientList = new ArrayList<>();
        try {
            ResultSet rs = db.executeQueries(sql);
            while (rs.next()) {
                Client client = new Client();
                client.setClientId(rs.getInt("client_id"));
                client.setName(rs.getString("name"));
                clientList.add(client);
            }
            rs.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientList;
    }
}
