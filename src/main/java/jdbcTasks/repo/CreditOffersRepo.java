package jdbcTasks.repo;

import jdbcTasks.db.Database;
import modelForCraditAgencyDb.CreditOffers;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class CreditOffersRepo extends FactoryMethod<CreditOffers> implements Repo<CreditOffers> {

    private final Database db;

    public CreditOffersRepo() {
        db = Database.getInstance();
    }

    @Override
    public CreditOffers readById(int id) {
        String sql = " SELECT * FROM credit_offers WHERE credit_id = " + id;
        return createListOfObjectsByQuery(sql).get(0);
    }

    @Override
    public List<CreditOffers> listAll() {
        String sql = " SELECT * FROM credit_offers";
        return createListOfObjectsByQuery(sql);
    }

    @Override
    public void insert(CreditOffers creditOffers) {
        String command = "INSERT INTO credit_agency.credit_offers (number_of_months, yearly_interest_rate) VALUES (?, ?)";
        try {
            PreparedStatement preparedStatement = db.getCon().prepareStatement(command, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, creditOffers.getNumberOfMonths());
            preparedStatement.setDouble(2, creditOffers.getYearlyInterestRate());
            preparedStatement.executeUpdate();
            db.commit();
            ResultSet keyResultSet = preparedStatement.getGeneratedKeys();
            if (keyResultSet.next()) {
                int newId = keyResultSet.getInt(1);
                creditOffers.setCreditId(newId);
            }
            preparedStatement.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(CreditOffers creditOffers) {
        String command = "UPDATE credit_agency.credit_offers set number_of_months = "
                + creditOffers.getNumberOfMonths() + "' WHERE credit_id = " + creditOffers.getCreditId();
        db.executeDMLCommand(command);
    }

    @Override
    public void delete(CreditOffers creditOffers) {
        String command = "delete from credit_agency.credit_offers WHERE credit_id = " + creditOffers.getCreditId();
        db.executeDMLCommand(command);
    }

    @Override
    List<CreditOffers> createListOfObjectsByQuery(String sql) {
        List<CreditOffers> creditOffersList = new ArrayList<>();
        try {
            ResultSet rs = db.executeQueries(sql);
            while (rs.next()) {
                CreditOffers creditOffer = new CreditOffers();
                creditOffer.setCreditId(rs.getInt("credit_id"));
                creditOffer.setNumberOfMonths(rs.getInt("number_of_months"));
                creditOffer.setYearlyInterestRate((double) rs.getFloat("yearly_interest_rate"));
                creditOffersList.add(creditOffer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return creditOffersList;
    }
}
