package jdbcTasks.repo;

import java.util.List;

public interface Repo<T> {

    T readById(int id);

    List<T> listAll();

    void insert(T t);

    void update(T t);

    void delete(T t);
}
