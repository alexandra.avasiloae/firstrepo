package jdbcTasks.repo;

import java.util.List;

public abstract class FactoryMethod<T> {
   abstract List<T> createListOfObjectsByQuery(String sql);
}
