package jdbcTasks;

import modelForCraditAgencyDb.Account;
import modelForCraditAgencyDb.Client;
import modelForCraditAgencyDb.CreditOffers;
import jdbcTasks.repo.AccountRepo;
import jdbcTasks.repo.ClientsRepo;
import jdbcTasks.repo.CreditOffersRepo;
import java.sql.SQLException;
import java.util.List;

public class Main {

    public static void main(String[] args) throws SQLException {

        Client client = new Client("Alex");
        Client client2 = new Client("User100");
        Client client3 = new Client("User2000");
        ClientsRepo clientsRepo = new ClientsRepo();
        //clientsRepo.insert(client3);

        List<Client> clients;
        clients = clientsRepo.listAll();
        System.out.println(clients);

        Client clientById = clientsRepo.readById(141);
        System.out.println(clientById);

        clientById.setName("Alexandra-Maria");
        clientsRepo.update(clientById);
        //clientsRepo.delete(clients.get(1));

        CreditOffers creditOffers1 = new CreditOffers(12, 12.5D);
        CreditOffers creditOffers2 = new CreditOffers(24, 10.2D);
        CreditOffers creditOffers3 = new CreditOffers(6, 15.2D);
        CreditOffersRepo creditOffersRepo = new CreditOffersRepo();

        //creditOffersRepo.insert(creditOffers3);

        List<CreditOffers> creditOffers;
        creditOffers = creditOffersRepo.listAll();
        System.out.println(creditOffers);

        CreditOffers creditOffersById = creditOffersRepo.readById(2);
        System.out.println(creditOffersById);
        // creditOffersRepo.delete(creditOffersById);

        AccountRepo accountRepo = new AccountRepo();

        //creditOffersRepo.insert(creditOffers3);

        List<Account> accountList;
        accountList=accountRepo.listAll();
        System.out.println(accountList);

        //CreditOffers creditOffersById = creditOffersRepo.readById(2);
        //System.out.println(creditOffersById);
    }
}
