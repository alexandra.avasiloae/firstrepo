package pairProgrammingChallenge.service;

import pairProgrammingChallenge.model.BankAccount;
import pairProgrammingChallenge.model.UserAccount;
import pairProgrammingChallenge.repo.BankRepo;
import pairProgrammingChallenge.repo.ObjFilePersistence;
import pairProgrammingChallenge.repo.UserRepo;
import pairProgrammingChallenge.utils.BankOperationException;
import pairProgrammingChallenge.utils.RateUtil;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class OpsService {
    private BankRepo bankRepo;
    private UserRepo userRepo;
    private RateUtil rateUtil;

    public OpsService(String bankPersistenceFileName, String userPersistenceFileName, String propertiesFilePath) {
        bankRepo = new BankRepo(new ObjFilePersistence(bankPersistenceFileName));
        userRepo = new UserRepo(new ObjFilePersistence(userPersistenceFileName));
        rateUtil = new RateUtil(propertiesFilePath);
    }


    public void deposit(UserAccount userAccount, double amount, int timeFrameInMonths) throws BankOperationException {
        if (userAccount.getBalance() >= amount) {
            userAccount.setBalance(userAccount.getBalance() - amount);

            BankAccount bankAccount = bankRepo.getBankAccount(userAccount.getBank());
            bankAccount.increaseBalance(amount);

            userRepo.update(userAccount);
            bankRepo.update(bankAccount);

            double finalAmount = amount + timeFrameInMonths * (rateUtil.getDepositInterestRate() / 100 * amount);

            Timer timer = new Timer();
            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    userAccount.setBalance(userAccount.getBalance() + finalAmount);
                    bankAccount.decreaseBalance(finalAmount);

                    userRepo.update(userAccount);
                    bankRepo.update(bankAccount);

                    List<BankAccount> bankAccounts = bankRepo.load();
                    System.out.println("\nUpdating...");
                    System.out.println(bankAccounts);

                    List<UserAccount> userAccounts = userRepo.load();
                    System.out.println(userAccounts);
                    System.out.println();

                    timer.cancel();
                }
            };

            timer.schedule(timerTask, timeFrameInMonths);
        } else {
            throw new BankOperationException("Insufficient funds");
        }
    }


    public void loan(UserAccount userAccount, double amount, int timeFrameInMonths) throws BankOperationException {
        BankAccount bankAccount = bankRepo.getBankAccount(userAccount.getBank());
        if (bankAccount.getBalance() >= amount) {
            userAccount.setBalance(userAccount.getBalance() + amount);
            userRepo.update(userAccount);

            bankAccount.decreaseBalance(amount);
            bankRepo.update(bankAccount);

            double amountPerMonth = amount / timeFrameInMonths + (rateUtil.getLoanInterestRate() / 100 * (amount / timeFrameInMonths));

            Timer timer = new Timer();

            TimerTask timerTask = new TimerTask() {
                @Override
                public void run() {
                    bankAccount.increaseBalance(amountPerMonth);
                    userAccount.setBalance(userAccount.getBalance() - amountPerMonth);
                    userRepo.update(userAccount);
                    bankRepo.update(bankAccount);

                    List<BankAccount> bankAccounts = bankRepo.load();
                    System.out.println("\nUpdating...");
                    System.out.println(bankAccounts);

                    List<UserAccount> userAccounts = userRepo.load();
                    System.out.println(userAccounts);
                    System.out.println();
                }
            };
            TimerTask timerTask2 = new TimerTask() {
                @Override
                public void run() {
                    timer.cancel();
                }
            };

            timer.scheduleAtFixedRate(timerTask, 500, 1000);
            timer.schedule(timerTask2, timeFrameInMonths * 1000L);
        } else {
            throw new BankOperationException("Bank doesn't have this amount available!");
        }

    }
}
