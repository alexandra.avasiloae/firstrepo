package pairProgrammingChallenge.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Properties;

public class RateUtil {

    private String filePath;
    private Properties props;

    public RateUtil(String filePath) {
        try (InputStream input = new FileInputStream(filePath)) {
            props = new Properties();
            props.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public double getDepositInterestRate() {
        if (props == null) {
            throw new NoSuchElementException();
        }
        return Double.parseDouble(props.getProperty("deposit.interest.rate"));
    }

    public double getLoanInterestRate() {
        if (props == null) {
            throw new NoSuchElementException();
        }
        return Double.parseDouble(props.getProperty("loan.interest.rate"));
    }
}
