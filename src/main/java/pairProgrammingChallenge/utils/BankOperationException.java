package pairProgrammingChallenge.utils;

public class BankOperationException extends Exception{

    public BankOperationException(String message){
        super(message);
    }
}
