package pairProgrammingChallenge;

import pairProgrammingChallenge.model.Bank;
import pairProgrammingChallenge.model.BankAccount;
import pairProgrammingChallenge.model.UserAccount;
import pairProgrammingChallenge.repo.BankRepo;
import pairProgrammingChallenge.repo.ObjFilePersistence;
import pairProgrammingChallenge.repo.UserRepo;
import pairProgrammingChallenge.service.OpsService;
import pairProgrammingChallenge.utils.BankOperationException;

import java.util.Arrays;
import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        UserRepo userRepo = new UserRepo(new ObjFilePersistence("src/main/resources/persistence/users.dat"));
        BankRepo bankRepo = new BankRepo(new ObjFilePersistence("src/main/resources/persistence/bank.dat"));

        Bank bank = new BankAccount("Mambu Bank", 7777, "MBM03");
        UserAccount userAccount = new UserAccount(1234, "John", "RO123456789", bank);

        BankAccount bankAccount = (BankAccount) bank;
        bankAccount.addClient(userAccount);
        bankAccount.increaseBalance(100000);

        userRepo.save(userAccount);
        bankRepo.save((BankAccount) bank);
        System.out.println(userAccount);

        System.out.println(bankAccount);
        System.out.println(bank);
        System.out.println(userAccount);
        System.out.println();

        OpsService service = new OpsService("src/main/resources/persistence/bank.dat",
                "src/main/resources/persistence/users.dat",
                "src/main/resources/config.properties");

        Scanner scanner = new Scanner(System.in);
        String menu = "\n- Deposit [accountNumber] [amount] [timeframe]\n- Loan [accountNumber] [amount] [timeframe]\n\n";
        while (true) {
            System.out.println(menu);
            String option = scanner.next();
            if (option.toLowerCase(Locale.ROOT).compareTo("exit") == 0) {
                break;
            }
            System.out.println(option);
            switch (option.toLowerCase(Locale.ROOT)) {
                case "deposit": {
                    String[] parameters = scanner.nextLine().trim().split("\\s+");
                    System.out.println(Arrays.toString(parameters));
                    String accountNumber = parameters[0];
                    try {
                        double amount = Double.parseDouble(parameters[1]);
                        int timeFrame = Integer.parseInt(parameters[2]);
                        UserAccount account = userRepo.getByAccountNumber(accountNumber);
                        try {
                            service.deposit(account, amount, timeFrame);
                        } catch (BankOperationException e) {
                            System.out.println(e.getMessage());
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("Invalid arguments, try again!");
                    }
                    break;
                }
                case "loan": {
                    String[] parameters = scanner.nextLine().trim().split("\\s+");
                    System.out.println(Arrays.toString(parameters));
                    String accountNumber = parameters[0];
                    try {
                        double amount = Double.parseDouble(parameters[1]);
                        int timeFrame = Integer.parseInt(parameters[2]);
                        UserAccount account = userRepo.getByAccountNumber(accountNumber);
                        System.out.println(account);
                        try {
                            service.loan(account, amount, timeFrame);
                        } catch (BankOperationException e) {
                            System.out.println(e.getMessage());
                        }
                    } catch (NumberFormatException ex) {
                        System.out.println("Invalid arguments, try again!");
                    }
                    break;
                }
                default:
                    System.out.println("Invalid command, try again!");
                    break;
            }
        }
    }
}
