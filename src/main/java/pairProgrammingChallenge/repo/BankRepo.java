package pairProgrammingChallenge.repo;

import pairProgrammingChallenge.model.Bank;
import pairProgrammingChallenge.model.BankAccount;

import java.util.List;

public class BankRepo extends AbstractRepo<BankAccount> {

    public BankRepo(ObjFilePersistence persistence) {
        super(persistence);
    }

    public BankAccount getBankAccount(Bank bank) {
        List<BankAccount> banks = load();
        return banks.stream().filter(b -> b.getId().compareTo(bank.getId()) == 0).findFirst().orElse(null);
    }
}
