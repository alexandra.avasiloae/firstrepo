package pairProgrammingChallenge.repo;

public record ObjFilePersistence(String persistenceFileName) {

    public String getPersistenceFileName() {
        return persistenceFileName;
    }
}
