package pairProgrammingChallenge.repo;

import pairProgrammingChallenge.model.UserAccount;

import java.util.List;

public class UserRepo extends AbstractRepo<UserAccount> {

    public UserRepo(ObjFilePersistence persistence) {
        super(persistence);
    }

    public UserAccount getByAccountNumber(String accountNumber) {
        List<UserAccount> userAccounts = load();
        System.out.println(userAccounts);
        return userAccounts.stream().filter(u -> u.getAccountNumber().compareTo(accountNumber) == 0).findFirst().orElse(null);
    }

}
