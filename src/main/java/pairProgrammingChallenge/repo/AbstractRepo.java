package pairProgrammingChallenge.repo;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepo<T> {
    protected final ObjFilePersistence persistence;

    public ObjFilePersistence getPersistence() {
        return persistence;
    }

    protected AbstractRepo(ObjFilePersistence persistence) {
        this.persistence = persistence;
    }

    public void save(T object) {
        try {
            List<T> data = load();
            data.add(object);
            FileOutputStream fileOut = new FileOutputStream(persistence.getPersistenceFileName());
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(data);
            objectOut.close();
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("Couldn't save object");
        }
    }

    public void update(T object) {
        List<T> userAccounts = load();
        System.out.println(userAccounts);

        for (T userAccount : userAccounts) {
            if (userAccount.equals(object)) {
                userAccounts.set(userAccounts.indexOf(userAccount), object);
            }
        }
        try {
            FileOutputStream fileOut = new FileOutputStream(persistence.getPersistenceFileName());
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(userAccounts);
            objectOut.close();
        } catch (IOException e) {
            System.out.println(e);
            System.out.println("Couldn't save object");
        }
    }

    public List<T> load() {
        try {
            FileInputStream fileIn = new FileInputStream(persistence.getPersistenceFileName());
            ObjectInputStream objectIn = new ObjectInputStream(fileIn);
            List<T> userAccounts = (List<T>) objectIn.readObject();
            objectIn.close();
            return userAccounts;
        } catch (IOException | ClassNotFoundException e) {
            return new ArrayList<>();
        }
    }
}
