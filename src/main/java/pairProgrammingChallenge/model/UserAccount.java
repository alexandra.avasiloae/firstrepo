package pairProgrammingChallenge.model;

import java.io.Serializable;
import java.util.Objects;

public class UserAccount implements Serializable {
    private final Integer id;
    private final String name;
    private final String accountNumber;
    private double balance;
    private final Bank bank;

    public UserAccount(Integer id, String name, String accountNumber, Bank bank) {
        this.id = id;
        this.name = name;
        this.accountNumber = accountNumber;
        this.bank = bank;
        this.balance = 0;
    }

    public String getName() {
        return name;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public double getBalance() {
        return balance;
    }

    public Bank getBank() {
        return bank;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        return "UserAccount{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserAccount that = (UserAccount) o;
        return id.equals(that.id) && accountNumber.equals(that.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, accountNumber);
    }
}
