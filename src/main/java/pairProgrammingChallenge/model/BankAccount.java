package pairProgrammingChallenge.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BankAccount extends Bank implements Serializable {
    private double balance;
    private final List<UserAccount> clients;

    public BankAccount(String name, Integer id, String code) {
        super(name, id, code);
        balance = 0;
        clients = new ArrayList<>();
    }

    public void addClient(UserAccount client) {
        clients.add(client);
    }

    public void increaseBalance(double amount) {
        balance += amount;
    }

    public void decreaseBalance(double amount) {
        balance -= amount;
    }

    public List<UserAccount> getClients() {
        return clients;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "balance=" + balance +
                '}';
    }
}
