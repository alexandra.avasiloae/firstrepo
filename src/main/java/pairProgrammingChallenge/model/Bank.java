package pairProgrammingChallenge.model;

import java.io.Serializable;
import java.util.Objects;

public class Bank implements Serializable {
    private final String name;
    private final Integer id;
    private final String code;

    public Bank(String name, Integer id, String code) {
        this.name = name;
        this.id = id;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", code='" + code + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bank bank = (Bank) o;
        return name.equals(bank.name) && id.equals(bank.id) && code.equals(bank.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id, code);
    }
}
