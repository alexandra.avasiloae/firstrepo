package fileOperations;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileOperations {

    private String filePath;

    public FileOperations(String filePath) {
        this.filePath = filePath;
    }

    public boolean createFile(){
        try {
            File myObj = new File(filePath);
            if (myObj.exists()) {
                return true;
            } else {
                return myObj.createNewFile();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean writeInFile(String toWrite) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(toWrite);
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    public String readFromFile() {
        try {
            List<String> lines = Files.readAllLines(Paths.get(filePath), StandardCharsets.UTF_8);
            return lines.toString().replace("[","").replace("]","");
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
