package generics;

public abstract class Media {

    private String name, gene;
    private int yearOfRelease;

    public Media(String name, String gene, int yearOfRelease) {
        this.name = name;
        this.gene = gene;
        this.yearOfRelease = yearOfRelease;
    }
}
