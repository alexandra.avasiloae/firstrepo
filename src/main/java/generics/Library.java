package generics;

import java.util.ArrayList;
import java.util.List;

public class Library < T extends Media> {

    private List<T> list = new ArrayList<>();

    public Library(List<T> list) {
        this.list = list;
    }

    public List<? extends Media> getList() {
        return list;
    }

    public void setList(List<T> list) {
        this.list = list;
    }
}
