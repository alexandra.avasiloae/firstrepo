package generics;

@FunctionalInterface
public interface Condition {
    boolean isApply(Integer elem);
}
