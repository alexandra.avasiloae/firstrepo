package generics;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static int countIf(Integer[] array, Condition condition) {
        int crt = 0;
        for (Integer integer : array) {
            if (condition.isApply(integer)) {
                crt++;
            }
        }
        return crt;
    }

    public static <T> void swap(T[] array, int position1, int position2) {
        try {
            T aux = array[position1];
            array[position1] = array[position2];
            array[position2] = aux;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {

        //Task 1
        Pair<String, Integer> ages1 = new Pair<>("Alexandra", 22);
        Pair<String, Integer> ages2 = new Pair<>("Ioana", 20);
        Pair<String, Integer> ages3 = new Pair<>("Andrei", 25);
        List<Pair<String, Integer>> listsOfPair = new ArrayList<>();
        listsOfPair.add(ages1);
        listsOfPair.add(ages2);
        listsOfPair.add(ages3);
        System.out.println(listsOfPair);

        //Task 2
        Integer[] array = {12, 1, 3, -4, 0, 6};

        System.out.println(countIf(array, e -> e < 0));
        System.out.println(countIf(array, new Condition() {
            @Override
            public boolean isApply(Integer elem) {
                return elem >= 0;
            }
        }));

        //Task 3
        String[] strings = {"ana", "maria", "jhon"};
        swap(strings, 1, 2);
        for (String s : strings) {
            System.out.println(s);
        }


        //Task 4

        Book book = new Book("Ion", "roman interbelic", 1922, "Liviu Rebreanu");
        Movie movie = new Movie("Thor", "action", 2022, "marvel");
        List<Media> list = new ArrayList<>();
        list.add(book);
        list.add(movie);
        Library<Media> library = new Library<>(list);

    }
}
