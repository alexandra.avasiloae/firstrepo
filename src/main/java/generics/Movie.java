package generics;

public class Movie extends Media {

    private String director;

    public Movie(String name, String gene, int yearOfRelease, String director) {
        super(name, gene, yearOfRelease);
        this.director = director;
    }
}
