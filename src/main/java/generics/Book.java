package generics;

public class Book extends Media {
    private String author;

    public Book(String name, String gene, int yearOfRelease, String author) {
        super(name, gene, yearOfRelease);
        this.author=author;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
