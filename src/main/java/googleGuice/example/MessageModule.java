package googleGuice.example;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;

public class MessageModule extends AbstractModule {
    @Provides
    @App.Message
    String provideMessage() {
        return "Hello, Guice!";
    }
}