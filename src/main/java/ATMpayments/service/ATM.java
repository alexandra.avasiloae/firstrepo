package ATMpayments.service;

import ATMpayments.model.User;

public interface ATM {

    boolean checkPin(User user, String pin);

    boolean userLog(User user);

    void payment(User user, double amount);

    void deposit(User user, double amount);

}
