package ATMpayments.service;

import ATMpayments.db.Database;
import ATMpayments.model.User;

public class ATMbcr implements ATM {

    private Database database;

    //used in production
    public ATMbcr() {
        this.database = Database.getInstance();
    }

    //used for tests
    public void setDatabase(Database database){
        this.database=database;
    }

    @Override
    public void payment(User user, double amount) {
        if (userLog(user)) {
            if (amount < user.getAccount().getBalance()) {
                user.getAccount().setBalance(user.getAccount().getBalance() - amount);
                database.updateBalance(user.getAccount().getBalance());
            }
        }
    }

    @Override
    public void deposit(User user, double amount) {
        if (amount < user.getAccount().getBalance()) {
            user.getAccount().setBalance(user.getAccount().getBalance() + amount);
            database.updateBalance(user.getAccount().getBalance());
        }
    }


    @Override
    public boolean userLog(User userToCheck) {
        if (database.getUser(userToCheck.getId()) == null) {
            return false;
        } else {
            User userFound = database.getUser(userToCheck.getId());
            return checkPin(userFound, userToCheck.getAccount().getPin());
        }
    }


    @Override
    public boolean checkPin(User user, String pin) {
        return pin.equals(user.getAccount().getPin());
    }
}