package ATMpayments.model;

import ATMpayments.model.Account;

public class User {

    private Account account;
    private String id;
    private String name;

    public User() {
    }

    public User(Account account, String id, String name) {
        this.account = account;
        this.id = id;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void addMoney(double amount) {
        this.account.setBalance(getAccount().getBalance() + amount);
    }
}
