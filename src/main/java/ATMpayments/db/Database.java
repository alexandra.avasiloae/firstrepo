package ATMpayments.db;

import ATMpayments.model.Account;
import ATMpayments.model.User;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;


public class Database {

    private static Database single_instance = null;
    private Connection con = null;
    private Statement stmt;
    private String sql;
    private ResultSet rs;


    private Database() {
        try {
            con = DriverManager.getConnection("jdbc:sqlite:identifier.sqlite");
            con.setAutoCommit(false);
            stmt = con.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public static Database getInstance() {
        if (single_instance == null)
            single_instance = new Database();
        return single_instance;
    }


    public void disconnect() {
        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public User getUser(String userId) {
        User user = new User();
        sql = " SELECT * FROM users where id=" + userId;
        try {
            rs = stmt.executeQuery(sql);
            user.setId(rs.getString("id"));
            user.setAccount(new Account(rs.getString("pin"), rs.getDouble("balance")));
            user.setName(rs.getString("nume"));
        } catch (Exception e) {
            return null;
        }
        return user;
    }

    public void updateBalance(double balance) {
        sql = "UPDATE users SET balance=" + balance;
        try {
            stmt.execute(sql);
            commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void commit() {
        try {
            con.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
