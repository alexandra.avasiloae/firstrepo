package jdbiTasks.DAO;

import modelForCraditAgencyDb.Currencies;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface CurrenciesDao {

    @SqlUpdate("insert into currencies (account_id, currencie, country) values (:account.accountId, :currencie, :country)")
    void insert(@BindBean Currencies currencies);

    @SqlUpdate("delete from credit_agency.currencies WHERE account_id = :account.accountId")
    void delete(@BindBean Currencies currencies);

    @SqlUpdate("UPDATE credit_agency.currencies set currencie = :currencie where account_id = :account.accountId")
    void update(@BindBean Currencies currencies);

    @SqlQuery("SELECT * FROM currencies")
    @RegisterBeanMapper(Currencies.class)
    List<Currencies> listAll();

    @SqlQuery("SELECT * FROM accounts where account_id = (?)")
    @RegisterBeanMapper(Currencies.class)
    Currencies currenciesById(Integer id);
}
