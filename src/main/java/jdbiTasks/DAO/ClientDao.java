package jdbiTasks.DAO;

import modelForCraditAgencyDb.Client;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;
import org.jdbi.v3.sqlobject.transaction.Transaction;

import java.util.List;

public interface ClientDao {

    @SqlUpdate("INSERT INTO clients (name) VALUES (:name)")
    @GetGeneratedKeys("client_id")
    int insertNamed(@Bind("name") String name);

    @SqlUpdate("INSERT INTO clients (name) VALUES (:name)")
    @GetGeneratedKeys("client_id")
    int insertBean(@BindBean Client client);

    @SqlUpdate("delete from credit_agency.clients WHERE client_id = :clientId")
    void delete(@BindBean Client client);

    @SqlUpdate("UPDATE credit_agency.clients set name = :name where client_id = :clientId")
    void update(@BindBean Client client);

    @SqlQuery("SELECT * FROM clients")
    @RegisterBeanMapper(Client.class)
    List<Client> listAll();

    @SqlQuery("SELECT * FROM clients where client_id = (?)")
    @RegisterBeanMapper(Client.class)
    Client clientById(Integer id);
}
