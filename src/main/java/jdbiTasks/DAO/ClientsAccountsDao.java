package jdbiTasks.DAO;

import modelForCraditAgencyDb.Account;
import modelForCraditAgencyDb.ClientsAccounts;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface ClientsAccountsDao {
    @SqlUpdate("insert into clients_accounts (id_client, id_account) values (:client.clientId , :account.accountId)")
    @GetGeneratedKeys("id")
    int insert(@BindBean ClientsAccounts clientsAccounts);

    @SqlUpdate("delete from credit_agency.clients_accounts WHERE id = :id")
    void delete(@BindBean ClientsAccounts clientsAccounts);

    @SqlQuery("SELECT * FROM clients_accounts")
    @RegisterBeanMapper(ClientsAccounts.class)
    List<Account> listAll();

    @SqlQuery("SELECT * FROM clients_accounts where id = (?)")
    @RegisterBeanMapper(ClientsAccounts.class)
    ClientsAccounts clientAccountById(Integer id);
}
