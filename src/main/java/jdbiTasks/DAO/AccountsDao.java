package jdbiTasks.DAO;

import modelForCraditAgencyDb.Account;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface AccountsDao {

    @SqlUpdate("insert into accounts (account_number, balance, credit_offer_id) values (:accountNumber, 0 , :creditOffer.creditId)")
    @GetGeneratedKeys("account_id")
    int insert(@BindBean Account account);

    @SqlUpdate("delete from credit_agency.accounts WHERE account_id = :accountId")
    void delete(@BindBean Account account);

    @SqlUpdate("UPDATE credit_agency.accounts set balance = :balance where account_id = :accountId")
    void update(@BindBean Account account);

    @SqlQuery("SELECT * FROM accounts")
    @RegisterBeanMapper(Account.class)
    List<Account> listAll();

    @SqlQuery("SELECT * FROM accounts where account_id = (?)")
    @RegisterBeanMapper(Account.class)
    Account accountById(Integer id);
}
