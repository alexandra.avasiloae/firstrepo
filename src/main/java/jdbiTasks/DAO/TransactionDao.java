package jdbiTasks.DAO;

import modelForCraditAgencyDb.Transaction;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface TransactionDao {

    @SqlUpdate("insert into transactions (transaction_id, transaction_type, amount) values (:clientsAccounts.id, :transactionType, :amount)")
    void insert(@BindBean Transaction transaction);

    @SqlUpdate("delete from credit_agency.transactions WHERE transaction_id = :clientsAccounts.id")
    void delete(@BindBean Transaction transaction);

    @SqlUpdate("UPDATE credit_agency.transactions set amount = :amount where transaction_id = :clientsAccounts.id")
    void update(@BindBean Transaction transaction);

    @SqlQuery("SELECT * FROM transactions")
    @RegisterBeanMapper(Transaction.class)
    List<Transaction> listAll();

    @SqlQuery("SELECT * FROM transactions where transaction _id = (?)")
    @RegisterBeanMapper(Transaction.class)
    Transaction currenciesById(Integer id);
}
