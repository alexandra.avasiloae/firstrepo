package jdbiTasks.DAO;

import modelForCraditAgencyDb.CreditOffers;
import org.jdbi.v3.sqlobject.config.RegisterBeanMapper;
import org.jdbi.v3.sqlobject.customizer.Bind;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.List;

public interface CreditOffersDao {

    @SqlUpdate("INSERT INTO credit_offers (number_of_months, yearly_interest_rate) VALUES (:number_of_months, :yearly_interest_rate)")
    @GetGeneratedKeys("credit_id")
    int insertNamed(@Bind("number_of_months") Integer numberOfMonths, @Bind("yearly_interest_rate") Double yearlyInterestRate);

    @SqlUpdate("INSERT INTO credit_offers (number_of_months, yearly_interest_rate) VALUES (:numberOfMonths, :yearlyInterestRate)")
    @GetGeneratedKeys("credit_id")
    int insertBean(@BindBean CreditOffers creditOffers);

    @SqlUpdate("delete from credit_agency.credit_offers WHERE credit_id = :creditId")
    void delete(@BindBean CreditOffers creditOffers);

    @SqlUpdate("UPDATE credit_agency.credit_offers set yearly_interest_rate = :yearlyInterestRate where credit_id = :creditId")
    void update(@BindBean CreditOffers creditOffers);

    @SqlQuery("SELECT * FROM credit_offers")
    @RegisterBeanMapper(CreditOffers.class)
    List<CreditOffers> listAll();

    @SqlQuery("SELECT * FROM credit_offers where credit_id = (?)")
    @RegisterBeanMapper(CreditOffers.class)
    CreditOffers clientById(Integer id);
}
