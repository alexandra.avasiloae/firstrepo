package jdbiTasks;

import jdbiTasks.DAO.AccountsDao;
import jdbiTasks.DAO.ClientDao;
import jdbiTasks.db.Database;
import modelForCraditAgencyDb.Account;
import modelForCraditAgencyDb.Client;
import org.jdbi.v3.core.Jdbi;

import java.util.List;

public class Main {

    public static void main(String[] args) {
        Database db = Database.getInstance();
        Jdbi jdbi = db.getJdbi();

//        Client client = jdbi.withExtension(ClientDao.class, dao->{
//            int client_id = dao.insertNamed("User2.0");
//            return dao.clientById(client_id);
//        });
//
//        System.out.println("Inserted client by name");
//        System.out.println(client);
//
        List<Client> allClients = jdbi.withExtension(ClientDao.class, ClientDao::listAll);
        System.out.println(allClients);
//
//        System.out.println("Inserted client by bean");
//        Client client2 = new Client("User2.1");
//
//        Client clientByBean = jdbi.withExtension(ClientDao.class, dao -> {
//            int client_id = dao.insertBean(client2);
//            return dao.clientById(client_id);
//        });
//        System.out.println(clientByBean);

        Client clientToUpdate = allClients.get(0);
        clientToUpdate.setName("UserUpdated");
        Client clientUpdated = jdbi.withExtension(ClientDao.class, dao -> {
            dao.update(clientToUpdate);
            return dao.clientById(clientToUpdate.getClientId());
        });

        System.out.println("Client updated: ");
        System.out.println(clientUpdated);

//        Client clientToDelete = allClients.get(5);
//        jdbi.withExtension(ClientDao.class, dao->{
//            dao.delete(clientToDelete);
//            return clientToDelete;
//        });
//
//        System.out.println("Client deleted: ");
//        System.out.println(clientToDelete);

//        CreditOffers creditOffers1 = jdbi.withExtension(CreditOffersDao.class, dao-> dao.listAll().get(0));
//        Account account = new Account("98765",creditOffers1);
//        Account accountToInsert = jdbi.withExtension(AccountsDao.class, dao->{
//            int generatedAccountId = dao.insert(account);
//            return dao.accountById(generatedAccountId);
//        });
//
        Account accountUpdated = jdbi.withExtension(AccountsDao.class, dao -> {
            Account account = dao.accountById(36);
            account.setBalance(1000D);
            dao.update(account);
            return dao.accountById(account.getAccountId());
        });
        System.out.println("Acccount updated balance");
        System.out.println(accountUpdated.getBalance());
    }
}
