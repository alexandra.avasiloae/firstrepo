package jdbiTasks.db;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;

public class Database {

    private final Jdbi jdbi;
    private static Database single_instance = null;

    private Database() {
        jdbi = Jdbi.create("jdbc:mysql://localhost:3306/credit_agency", "root", "my-secret-pw");
        jdbi.installPlugin(new SqlObjectPlugin());
    }

    public static Database getInstance() {
        if (single_instance == null)
            single_instance = new Database();
        return single_instance;
    }

    public Jdbi getJdbi() {
        return jdbi;
    }
}
