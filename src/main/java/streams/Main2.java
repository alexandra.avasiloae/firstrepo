package streams;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Main2 {

    public static List<Episode> allEpisodes(List<Season> seasonList) {
        return seasonList.stream()
                .flatMap(season -> season.episodes.stream())
                .collect(Collectors.toList());

    }

    public static List<Video> allVideos(List<Season> seasonList) {
        return seasonList.stream()
                .flatMap(season -> season.episodes.stream()
                        .flatMap(episode -> episode.videos.stream()))
                .collect(Collectors.toList());
    }

    public static List<String> allSeasonNames(List<Season> seasonList) {
        return seasonList.stream()
                .map(season -> season.seasonName)
                .collect(Collectors.toList());
    }

    public static List<Integer> allSeasonNumbers(List<Season> seasonList) {
        return seasonList.stream()
                .map(season -> season.seasonNumber)
                .collect(Collectors.toList());
    }

    public static List<String> allEpisodesNames(List<Season> seasonList) {
        return seasonList.stream()
                .flatMap(season -> season.episodes.stream()
                        .map(ep -> ep.episodeName))
                .collect(Collectors.toList());
    }

    public static List<Integer> allEpisodesNumbers(List<Season> seasonList) {
        return seasonList.stream()
                .flatMap(season -> season.episodes.stream()
                        .map(ep -> ep.episodeNumber))
                .collect(Collectors.toList());
    }

    public static List<String> allVideosNames(List<Season> seasonList) {
        return seasonList.stream()
                .flatMap(season -> season.episodes.stream()
                        .flatMap(eps -> eps.videos.stream()
                                .map(video -> video.title)))
                .collect(Collectors.toList());
    }

    public static List<String> allVideosUrl(List<Season> seasonList) {
        return seasonList.stream()
                .flatMap(season -> season.episodes.stream()
                        .flatMap(eps -> eps.videos.stream()
                                .map(video -> video.url)))
                .collect(Collectors.toList());
    }

    public static List<Episode> allEpisodesFromEvenSeason(List<Season> seasonList) {
        return seasonList.stream()
                .filter(season -> season.seasonNumber % 2 == 0)
                .flatMap(season -> season.episodes.stream())
                .collect(Collectors.toList());
    }

    public static List<Video> allVideosFromEvenSeason(List<Season> seasonList) {
        return seasonList.stream()
                .filter(season -> season.seasonNumber % 2 == 0)
                .flatMap(season -> season.episodes.stream()
                        .flatMap(eps -> eps.videos.stream()))
                .collect(Collectors.toList());
    }

    public static List<Video> allVideosFromEvenEpisodeAndSeason(List<Season> seasonList) {
        return seasonList.stream()
                .filter(season -> season.seasonNumber % 2 == 0)
                .flatMap(season -> season.episodes.stream()
                        .filter(eps -> eps.episodeNumber % 2 == 0)
                        .flatMap(eps -> eps.videos.stream()))
                .collect(Collectors.toList());
    }

    public static List<Video> onlyClipFromEvenEpisodeAndOddSeason(List<Season> seasonList) {
        return seasonList.stream()
                .filter(seaon -> seaon.seasonNumber % 2 != 0)
                .flatMap(season -> season.episodes.stream()
                        .filter(eps -> eps.episodeNumber % 2 == 0)
                        .flatMap(eps -> eps.videos.stream()
                                .filter(video -> video.videoType.equals(VideoType.CLIP))))
                .collect(Collectors.toList());
    }

    public static List<Video> onlyPreviewFromOddEpisodeAndEvenSeason(List<Season> seasonList) {
        return seasonList.stream()
                .filter(seaon -> seaon.seasonNumber % 2 == 0)
                .flatMap(season -> season.episodes.stream()
                        .filter(eps -> eps.episodeNumber % 2 != 0)
                        .flatMap(eps -> eps.videos.stream()
                                .filter(video -> video.videoType.equals(VideoType.PREVIEW))))
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {

        Video video1 = new Video("V1", "url1", VideoType.CLIP);
        Video video2 = new Video("V2", "url2", VideoType.EPISODE);
        Video video3 = new Video("V3", "url3", VideoType.PREVIEW);
        Video video4 = new Video("V4", "url4", VideoType.EPISODE);
        Video video5 = new Video("V5", "url5", VideoType.CLIP);
        Video video6 = new Video("V6", "url6", VideoType.PREVIEW);
        Video video7 = new Video("V7", "url7", VideoType.CLIP);


        List<Video> videoList1 = new ArrayList<>();
        videoList1.add(video1);
        videoList1.add(video2);
        videoList1.add(video3);


        List<Video> videoList2 = new ArrayList<>();
        videoList2.add(video4);
        videoList2.add(video5);

        List<Video> videoList3 = new ArrayList<>();
        videoList3.add(video6);
        videoList3.add(video7);

        Episode episode1 = new Episode("Ep1", 1, videoList1);
        Episode episode2 = new Episode("Ep2", 2, videoList2);
        List<Episode> episodes = new ArrayList<>();
        episodes.add(episode1);
        episodes.add(episode2);

        Episode episode3 = new Episode("Ep3", 4, videoList3);

        List<Episode> episodes1 = new ArrayList<>();
        episodes1.add(episode3);

        Season season1 = new Season("S1", 1, episodes);
        Season season2 = new Season("S2", 2, episodes1);

        List<Season> seasonList = new ArrayList<>();
        seasonList.add(season1);
        seasonList.add(season2);


        //1
        System.out.println("All episodes");
        System.out.println(allEpisodes(seasonList));

        //2
        System.out.println("All videos");
        System.out.println(allVideos(seasonList));

        //3
        System.out.println("All season names");
        System.out.println(allSeasonNames(seasonList));

        //4
        System.out.println("All season numbers");
        System.out.println(allSeasonNumbers(seasonList));

        //5
        System.out.println("All episodes names");
        System.out.println(allEpisodesNames(seasonList));

        //6
        System.out.println("All episodes numbers");
        System.out.println(allEpisodesNumbers(seasonList));


        //8
        System.out.println("All videos names");
        System.out.println(allVideosNames(seasonList));

        //9
        System.out.println("All videos url");
        System.out.println(allVideosUrl(seasonList));

        //10
        System.out.println("Only episodes from even season");
        System.out.println(allEpisodesFromEvenSeason(seasonList));

        //11
        System.out.println("Only videos from even season");
        System.out.println(allVideosFromEvenSeason(seasonList));

        //12
        System.out.println("Only videos from even episodes and seasons");
        System.out.println(allVideosFromEvenEpisodeAndSeason(seasonList));

        //13
        System.out.println("Only Clip videos from odd season and even episode");
        System.out.println(onlyClipFromEvenEpisodeAndOddSeason(seasonList));

        //14
        System.out.println("Only Priview videos from even season and odd episode");
        System.out.println(onlyPreviewFromOddEpisodeAndEvenSeason(seasonList));


    }
}
