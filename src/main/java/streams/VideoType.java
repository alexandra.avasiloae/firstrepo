package streams;

public enum VideoType {
    CLIP, PREVIEW, EPISODE
}
