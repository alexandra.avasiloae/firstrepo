package streams;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {

        //1
        List<String> stringList = new ArrayList<>();
        stringList.add("abc");
        stringList.add("bbb");
        stringList.add("abcd");
        stringList.stream().filter(string -> string.length() == 3 && string.startsWith("a")).collect(Collectors.toList()).forEach(System.out::println);

        //2
        List<Integer> integerList0 = new LinkedList<>();
        integerList0.add(1);
        integerList0.add(5);
        integerList0.add(8);
        int i = integerList0.stream().reduce(0, (sum, element) -> sum += element) / integerList0.size();
        System.out.println(i);


        AtomicReference<Integer> sum = new AtomicReference<>(0);
        integerList0.forEach(element -> sum.updateAndGet(v -> v + element));


        //3
        List<String> strings1 = new ArrayList<>();
        strings1.add("abc");
        strings1.add("efg");
        strings1.add("ABC");

        List<String> stringsUpperCase = strings1.stream().map(string -> string.toUpperCase(Locale.ROOT)).collect(Collectors.toList());
        System.out.println(stringsUpperCase);

        //4
        List<Integer> integerList = new ArrayList<>();
        integerList.add(3);
        integerList.add(44);
        String result = integerList.stream().map(integer -> integer % 2 == 0 ? "e" + integer.toString() : "o" + integer.toString()).reduce("", (acc, word) -> acc += (acc.isEmpty() ? "" : ",") + word);
        System.out.println(result);

        //5
        List<Employee> employees = new ArrayList<>();
        Employee employee = new Employee("12345", "Alexandra", 1000.0);
        Employee employee1 = new Employee("12346", "Alexandru", 10000.0);
        Employee employee2 = new Employee("12347", "Ana", 100000.0);

        employees.add(employee);
        employees.add(employee1);
        employees.add(null);
        employees.add(employee2);

        Employee employeeList = employees.stream().filter(employee3 -> employee3 != null && employee3.getSalary() >= 30000).findFirst().orElseThrow(NullPointerException::new);
        System.out.println(employeeList);

        //6
        Employee employee4 = new Employee("1", "Name", 300D);
        Employee employee5 = new Employee("2", "Otherame", 500D);
        Employee employee6 = new Employee("3", "AnotherName", 300D);
        List<Employee> employeeList1 = new ArrayList<>();
        employeeList1.add(employee);
        employeeList1.add(employee1);
        employeeList1.add(employee2);
        List<Employee> employees1 = employeeList1.stream().sorted(Comparator.comparing(Employee::getName)).collect(Collectors.toList());
        System.out.println(employees1);

    }
}
