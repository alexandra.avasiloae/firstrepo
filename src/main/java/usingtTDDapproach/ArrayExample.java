package usingtTDDapproach;

import java.util.Arrays;
import java.util.LinkedHashSet;

public class ArrayExample {

    public static String[] removeDuplicates(String[] array) {
        LinkedHashSet<String> noDuplicates = new LinkedHashSet<String>(Arrays.asList(array));

        String[] newArray = new String[noDuplicates.size()];

        return noDuplicates.toArray(newArray);
    }
}
