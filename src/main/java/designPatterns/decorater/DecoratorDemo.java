package designPatterns.decorater;

import java.util.Arrays;

//ex 7 - DP - decorator
public class DecoratorDemo {
    public static void main(String[] args) {
        final StatisticsLogger statisticsLogger = new WithMeanStatisticsLogger(
                new WithSummaryStatisticsLogger(
                        new ExecutionTimesBaseStatistics(Arrays.asList(1.2, 2.2, 3.4))));
        statisticsLogger.displayStatistics();
    }
}