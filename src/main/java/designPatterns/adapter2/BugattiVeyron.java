package designPatterns.adapter2;

public class BugattiVeyron implements Movable {

    @Override
    public double getSpeedMilesPerHour() {
        return 268;
    }
}