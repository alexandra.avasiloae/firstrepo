package designPatterns.adapter2;

public class MovableAdapterImpl implements MovableAdapter {
    private Movable luxuryCars;

    public MovableAdapterImpl(Movable movable) {
        this.luxuryCars = movable;
    }

    @Override
    public double getSpeedKilometersPerHour() {
        return convertMPHtoKMPH(luxuryCars.getSpeedMilesPerHour());
    }

    private double convertMPHtoKMPH(double mph) {
        return mph * 1.60934;
    }
}