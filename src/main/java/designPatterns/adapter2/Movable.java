package designPatterns.adapter2;

public interface Movable {
    // returns speed in MPH
    double getSpeedMilesPerHour();
}