package designPatterns.adapter2;

public interface MovableAdapter {
    // returns speed in KM/H 
    double getSpeedKilometersPerHour();
}