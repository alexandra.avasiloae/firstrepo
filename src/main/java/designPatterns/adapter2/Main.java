package designPatterns.adapter2;

public class Main {
    public static void main(String[] args) {
        BugattiVeyron car = new BugattiVeyron();
        System.out.println(car.getSpeedMilesPerHour());

        MovableAdapter adapter = new MovableAdapterImpl(car);
        System.out.println(adapter.getSpeedKilometersPerHour());
    }
}
