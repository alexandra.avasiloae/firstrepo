package designPatterns.observer;

public class Observer {
    public void receivedValue(double value) {
        System.out.println("Received value " + value);
    }
}
