package designPatterns.observer;

import java.util.ArrayList;

public class Observable {
    private double someDoubleValue;

    private ArrayList<Observer> observers = new ArrayList<>();

    public void subscribeToCallbacks(Observer observer) {
        this.observers.add(observer);
    }

    public void removeFromCallbacks(Observer observer) {
        this.observers.remove(observer);
    }

    public void setSomeDoubleValue(double someDoubleValue) {
        this.someDoubleValue = someDoubleValue;
        for (Observer observer : this.observers) {
            observer.receivedValue(someDoubleValue);
        }
    }
}
