package designPatterns.observer;

public class Main {
    public static void main(String[] args) {

        Observer observer = new Observer();
        Observable observable = new Observable();
        observable.setSomeDoubleValue(20);
        observable.setSomeDoubleValue(15);
        observable.subscribeToCallbacks(observer);
        observable.setSomeDoubleValue(35);
        observable.setSomeDoubleValue(40);
        observable.removeFromCallbacks(observer);
        observable.setSomeDoubleValue(20);
    }
}
