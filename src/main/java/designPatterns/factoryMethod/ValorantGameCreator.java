package designPatterns.factoryMethod;

public class ValorantGameCreator extends GameFactory {
    @Override
    Game create() {
        return new PCGame();
    }
}