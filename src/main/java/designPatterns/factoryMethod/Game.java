package designPatterns.factoryMethod;

public abstract class Game {
    abstract String getName();
    abstract String getType();
}