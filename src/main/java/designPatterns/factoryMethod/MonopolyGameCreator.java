package designPatterns.factoryMethod;

public class MonopolyGameCreator extends GameFactory {
    @Override
    Game create() {
        return new BoardGame();
    }
}