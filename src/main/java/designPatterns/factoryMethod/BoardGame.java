package designPatterns.factoryMethod;

public class BoardGame extends Game {
    @Override
    String getName() {
        return "Some Board Game";
    }

    @Override
    String getType() {
        return "Board game";
    }
}