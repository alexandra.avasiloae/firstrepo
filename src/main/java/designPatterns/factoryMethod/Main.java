package designPatterns.factoryMethod;

public class Main {

    public static void main(String[] args) {

        GameFactory creator = new MonopolyGameCreator();
        Game someGame = creator.create();
        System.out.println(someGame.getName());
    }
}
