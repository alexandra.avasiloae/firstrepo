package designPatterns.factoryMethod;

public abstract class GameFactory {
    abstract Game create();
}