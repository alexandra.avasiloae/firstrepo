package designPatterns.factoryMethod;

public class PCGame extends Game{
    @Override
    String getName() {
        return "Some PC Game";
    }

    @Override
    String getType() {
        return "PC game";
    }
}