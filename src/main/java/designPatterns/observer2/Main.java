package designPatterns.observer2;

public class Main {

    public static void main(String[] args) {

        Subject subject = new Subject();

        Observer o1 = new SpecificValueObserver(subject);
        Observer o2 = new ByTenChangedObserver(subject);
        Observer o3 = new ValueLoweredObserver(subject);

        subject.subscribe(o1);
        subject.subscribe(o2);
        subject.subscribe(o3);

        subject.changeStateBy(20);
        subject.changeStateBy(10);
        subject.changeStateBy(1);
    }
}
