package designPatterns.flyweight;

import java.awt.*;

public interface Vehicle {
    public void start();
    public void stop();
    public Color getColor();
}
