package designPatterns.adapter;

import designPatterns.singleton.Requirements;

public interface PCGame {
    String getTitle();

    Integer getPegiAllowedAge();

    boolean isTripleAGame();

    Requirements getRequirements();
}
