package designPatterns.adapter;

public class Main {

    public static void main(String[] args) {

        //adapter
        ComputerGame computerGame = new ComputerGame("LoL", PegiAgeRating.P3, 5D, 10, 2, 3, 4, 1000D);
        ComputerGameAdapter computerGameAdapter = new ComputerGameAdapter(computerGame);
        System.out.println(computerGameAdapter.getTitle());
        System.out.println(computerGameAdapter.getPegiAllowedAge());
        System.out.println(computerGameAdapter.getRequirements().getCoresNum());
        System.out.println(computerGameAdapter.isTripleAGame());
    }
}
