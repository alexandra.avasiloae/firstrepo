package designPatterns.singleton;

public class Main {

    public static void main(String[] args) {

        //lazy singleton
        Servers server = Servers.getInstance();
        server.addServer("http://");
        System.out.println(server.getHttpServers());

        //eager singleton
        Servers2 servers2 = Servers2.getInstance();
        servers2.addServer("https://");
        System.out.println(server.getHttpsServers());
    }
}
