package designPatterns.singleton;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


//eager singleton
public class Servers2 {

    private static final Servers2 INSTANCE = new Servers2();

    public static Servers2 getInstance() {
        return INSTANCE;
    }

    private final List<String> serverList;

    private Servers2() {
        serverList = new ArrayList<>();
    }

    public boolean addServer(final String server) {
        if ((server.startsWith("http") || server.startsWith("https")) && !serverList.contains(server)) {
            return serverList.add(server);
        }
        return false;
    }

    public List<String> getHttpServers() {
        return serverList.stream()
                .filter(server -> server.startsWith("http"))
                .collect(Collectors.toList());
    }

    private List<String> getServersStartingWith(final String prefix) {
        return serverList.stream()
                .filter(server -> server.startsWith(prefix))
                .collect(Collectors.toList());
    }

    public List<String> getHttpsServers() {
        return serverList.stream()
                .filter(server -> server.startsWith("https"))
                .collect(Collectors.toList());
    }
}