package designPatterns.builder;

public class Main {

    public static void main(String[] args) {
        //builder
        Dog dog = new Dog.Builder()
                .withName("Oscar")
                .withAge(3)
                .withType("Maidanez")
                .build();

        System.out.println(dog);
    }
}
