package designPatterns.proxy;

public interface ExpensiveObject {
    void process();
}