package designPatterns.cor.chainOfResponsability;

import designPatterns.cor.model.Transaction;

public abstract class AuthenticationProcessor {

    private AuthenticationProcessor nextProcessor;

    public void setNextProcessor(AuthenticationProcessor nextProcessor) {
        this.nextProcessor = nextProcessor;
    }

    public void validate(Transaction transaction){
        if(nextProcessor != null)
            nextProcessor.validate(transaction);
    };

    public abstract void process(Transaction transaction);
}
