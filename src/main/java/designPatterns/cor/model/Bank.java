package designPatterns.cor.model;

import java.util.ArrayList;
import java.util.List;

public class Bank {

    private List<Account> accountList = new ArrayList<>();

    public Bank(List<Account> accountList) {
        this.accountList = accountList;
    }

    public List<Account> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<Account> accountList) {
        this.accountList = accountList;
    }
}
