package collections;

public enum SortBooksBy {
    AUTHORS_NUMBER,
    PRICE,
    TITLE,
    YEAR
}
