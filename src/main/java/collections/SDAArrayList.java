package collections;

import java.util.Arrays;

public class SDAArrayList<E> {

    private Object[] listOfElem;
    private int size;

    public SDAArrayList() {
        listOfElem = new Object[1];
    }

    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bound");
        } else {
            return (E) listOfElem[index];
        }
    }

    public void add(E elem) {

        if (size == listOfElem.length) {
            int capacity = listOfElem.length * 2;
            listOfElem = Arrays.copyOf(listOfElem, capacity);
        }
        listOfElem[size++] = elem;

    }

    public void remove(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index out of bound");
        }

        Object removedElement = listOfElem[index];
        if (size - 1 - index >= 0) System.arraycopy(listOfElem, index + 1, listOfElem, index, size - 1 - index);
        size--;
        listOfElem = Arrays.copyOf(listOfElem, listOfElem.length - 1);
    }

    public void display() {
        for (Object elem : listOfElem) {
            System.out.println(elem);
        }
    }

}
