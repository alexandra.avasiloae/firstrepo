package collections;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        //Task 1
        SDAArrayList<String> list = new SDAArrayList<>();
        list.add("alexandra");
        list.add("ioana");
        list.display();
        System.out.println();
        list.remove(1);
        list.display();
        System.out.println();
        System.out.println(list.get(0));

        //Task 2,4,5
        Author author1 = new Author("Margaret", "Mitchell", "female");
        Author author2 = new Author("M.", "Mitchell", "female");
        List<Author> listOfAuthor1 = new ArrayList<>();
        listOfAuthor1.add(author1);
        listOfAuthor1.add(author2);

        Author author3 = new Author("Liviu", "Rebreanu", "male");
        List<Author> authorList2 = new ArrayList<>();
        authorList2.add(author3);


        Book book1 = new Book("Gone with the wind", 30.5, 1936, listOfAuthor1, Genre.DRAMA);
        Book book2 = new Book("Ion", 20, 1912, authorList2, Genre.CLASSIC);

        BookService bookService = new BookService();
        bookService.addBook(book1);
        bookService.addBook(book2);
        System.out.println(bookService.getBooks().toString());

        System.out.println(bookService.cheapestBook());

        System.out.println(bookService.sortedListOfBooks(SortBooksBy.TITLE, SortedType.DESCENDING).toString());
        System.out.println(bookService.sortedListOfBooks(SortBooksBy.AUTHORS_NUMBER, SortedType.ASCENDING).toString());

        System.out.println(bookService.returningBooksByAuthor(author1));

        System.out.println(bookService.getPairs());

        System.out.println(bookService.getStackSortedByPrice());


        //Task 3
        Integer[] array = new Integer[100];
        int max = 50, min = 0;
        for (int i = 0; i < array.length; i++) {
            array[i] = new Random().nextInt(max - min + 1) + min;
        }

        Set<Integer> uniqueElements = new HashSet<>();
        Set<Integer> duplicatedElements = new HashSet<>();
        for (Integer value : array) {
            if (!uniqueElements.add(value)) {
                duplicatedElements.add(value);
            }
        }
        System.out.println(uniqueElements);
        System.out.println(duplicatedElements);
    }
}
