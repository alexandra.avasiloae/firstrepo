package collections;

public enum Genre {
    CLASSIC,
    ACTION,
    DRAMA,
    CRIME,
    FANTASY
}
