package collections;

import java.util.*;
import java.util.stream.Collectors;

public class BookService {

    private List<Book> books;

    public BookService() {
        books = new ArrayList<>();
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }


    public void addBook(Book book) {
        this.books.add(book);
    }

    public void removeBook(Book book) {
        this.books.remove(book);
    }

    public List<Book> returningListOfFantasyBooks() {
        return books.stream().filter(book -> book.getGenre().equals(Genre.FANTASY)).collect(Collectors.toList());
    }

    public List<Book> returningListOfBooksBefore1999() {
        return books.stream().filter(book -> book.getYearOfRelease() < 1999).collect(Collectors.toList());
    }

    public Optional<Book> mostExpensiveBook() {
        return books.stream().max(Comparator.comparing(Book::getPrice));
    }

    public Optional<Book> cheapestBook() {
        return books.stream().min(Comparator.comparing(Book::getPrice));
    }

    public List<Book> getBooksWithNumberOfAuthors(int numberOfAuthors) {
        List<Book> newList = new ArrayList<>();
        for (Book book : books) {
            if (book.getAuthorList().size() == numberOfAuthors) {
                newList.add(book);
            }
        }
        return newList;
    }

    public List<Book> sortedListOfBooks(SortBooksBy sortBooksBy, SortedType sortedType) {
        List<Book> sortedList = this.books;

        Comparator<Book> comparator = null;


        if (sortBooksBy.equals(SortBooksBy.TITLE)) {
            comparator = Comparator.comparing(Book::getTitle);
        }
        if (sortBooksBy.equals(SortBooksBy.YEAR)) {
            comparator = Comparator.comparing(Book::getYearOfRelease);
        }
        if (sortBooksBy.equals(SortBooksBy.PRICE)) {
            comparator = Comparator.comparing(Book::getPrice);
        }
        if (sortBooksBy.equals(SortBooksBy.AUTHORS_NUMBER)) {
            comparator = Comparator.comparing(b -> b.getAuthorList().size());
        }


        if (sortedType.equals(SortedType.ASCENDING)) {
            sortedList = sortedList.stream().sorted(comparator).collect(Collectors.toList());
        } else {
            sortedList = sortedList.stream().sorted(comparator.reversed()).collect(Collectors.toList());
        }


        return sortedList;
    }

    public boolean verifyABook(Book book) {
        return books.contains(book);
    }


    public List<Book> returningBooksByAuthor(Author author) {
        List<Book> newList;
        newList = books.stream().filter(a -> a.getAuthorList().contains(author)).collect(Collectors.toList());
        return newList;

    }

    public Map<Genre, String> getPairs() {
        Map<Genre, String> map = new HashMap<>();
        for (Book book : books) {
            map.put(book.getGenre(), book.getTitle());
        }
        return map;
    }

    public Stack<Book> getStackSortedByPrice() {
        Stack<Book> sortedStack = new Stack<>();

        List<Book> sortedListByPrice = sortedListOfBooks(SortBooksBy.PRICE, SortedType.DESCENDING);
        sortedStack.addAll(sortedListByPrice);
        return sortedStack;
    }
}
