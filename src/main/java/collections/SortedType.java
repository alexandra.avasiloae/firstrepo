package collections;

public enum SortedType {
    ASCENDING,
    DESCENDING
}
