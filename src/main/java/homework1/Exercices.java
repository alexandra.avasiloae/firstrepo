package homework1;

import java.util.ArrayList;
import java.util.List;

public class Exercices {
    /**
     * method that convert celsius to fahrenheit
     *
     * @param celsius
     * @return
     */
    public static double convertCToF(double celsius) {
        double fahrenheit;
        fahrenheit = (celsius * 1.8) + 32;
        return fahrenheit;
    }

    /**
     * method to convert fahrenheit to celsius
     *
     * @param fahrenheit
     * @return
     */
    public static double convertFToC(double fahrenheit) {
        double celsius;
        celsius = (fahrenheit - 32) / 1.8;
        return celsius;
    }

    /**
     * this method find the maximum values of 3 numbers
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public static double maxOf3(double a, double b, double c) {
        return Math.max(Math.max(a, b), c);
    }

    /**
     * this methos checks if a value is prime or not
     *
     * @param value
     * @return
     */
    public static boolean isPrime(double value) {
        if (value <= 1)
            return false;
        if (value % 2 == 0 && value != 2) {
            return false;
        }
        for (int i = 3; i < value / 2; i++)
            if (value % i == 0)
                return false;

        return true;
    }

    /**
     * this method returns the mirrored of a String
     *
     * @param text
     * @return
     */
    public static String reverseString(String text) {
        StringBuilder mirrored = new StringBuilder();
        for (int i = text.length() - 1; i >= 0; i--) {
            mirrored.append(text.charAt(i));
        }
        return mirrored.toString();
    }

    /**
     * this method checks if a String is palindrome or not
     *
     * @param text
     * @return
     */
    public static boolean isPalindrome(String text) {
        String mirrored = reverseString(text);
        return text.equals(mirrored);
    }

    /**
     * this method return how many days, hours and minutes are
     *
     * @param n
     * @return
     */
    public static String convertSeconds(int n) {
        int days, h, min;
        String text = "";
        days = n / 86400;
        int secondsRemained = n % 86400;
        h = secondsRemained / 3600;
        secondsRemained = secondsRemained % 3600;
        min = secondsRemained / 60;
        secondsRemained = secondsRemained % 60;

        text += days;
        text += " days ";
        text += h;
        text += " h ";
        text += min;
        text += " min ";
        text += secondsRemained;
        text += " sec";
        return text;
    }

    /**
     * this method try to convert a String to a float
     * if it is not possible returns null
     *
     * @param str
     * @return
     */
    public static Object convertStringToFloat(String str) {
        try {
            return Float.parseFloat(str);
        } catch (Exception e) {
            return null;
        }
    }

    public static int nThPrime(int n) {
        int max = 10000;
        List<Integer> primes = new ArrayList<Integer>();
        for (int i = 2; i < max; i++) {
            if (isPrime(i)) {
                primes.add(i);
            }
        }
        return primes.get(n - 1);
    }

    public static void main(String[] args) {

        //Ex 1 and 2
        double celsius = 25;
        double fahrenheit = convertCToF(celsius);
        System.out.println(celsius + " C = " + fahrenheit + " F");
        System.out.println(fahrenheit + " F = " + convertFToC(fahrenheit) + " C");

        //Ex 3
        double a = 12.3, b = -5.1, c = 0.9;
        System.out.println("Maximum of " + a + ", " + b + " and " + c + " is " + maxOf3(a, b, c));

        //Ex 4
        double value1 = 2, value2 = 3, value3 = 7, value4 = 15;
        if (isPrime(value1)) {
            System.out.println(value1 + " is prime.");
        } else {
            System.out.println(value1 + "is not prime.");
        }

        if (isPrime(value2)) {
            System.out.println(value2 + " is prime.");
        } else {
            System.out.println(value2 + "is not prime.");
        }
        if (isPrime(value3)) {
            System.out.println(value3 + " is prime.");
        } else {
            System.out.println(value3 + "is not prime.");
        }
        if (isPrime(value4)) {
            System.out.println(value4 + " is prime.");
        } else {
            System.out.println(value4 + " is not prime.");
        }

        //Ex 5, 6
        String text = "abcde";
        String text1 = "abcdeedcba";
        System.out.println("Mirrored version of '" + text + "' is '" + reverseString(text) + "'.");

        if (isPalindrome(text)) {
            System.out.println(text + "is palindrome");
        } else {
            System.out.println(text + "is not palindrome");
        }
        if (isPalindrome(text1)) {
            System.out.println(text1 + " is palindrome");
        } else {
            System.out.println(text1 + " is not palindrome");
        }

        //Ex 7
        int numberOfSeconds = 1234567;
        System.out.println(numberOfSeconds + " = " + convertSeconds(numberOfSeconds));


        //Ex 8
        System.out.println(convertStringToFloat("3.1"));
        System.out.println(convertStringToFloat("alexandra"));

        //Ex 9
        System.out.println(nThPrime(5));
    }

}
