package modelForCraditAgencyDb;

import org.jdbi.v3.core.mapper.Nested;

import java.util.Objects;

public class Transaction {

    @Nested
    private ClientsAccounts clientsAccounts;
    private String transactionType;
    private Double amount;

    public Transaction() {
    }

    public Transaction(ClientsAccounts clientsAccounts, String transactionType, Double amount) {
        this.clientsAccounts = clientsAccounts;
        this.transactionType = transactionType;
        this.amount = amount;
    }

    public ClientsAccounts getClientsAccounts() {
        return clientsAccounts;
    }

    public void setClientsAccounts(ClientsAccounts clientsAccounts) {
        this.clientsAccounts = clientsAccounts;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "clientsAccounts=" + clientsAccounts +
                ", transactionType='" + transactionType + '\'' +
                ", amount=" + amount +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Objects.equals(clientsAccounts, that.clientsAccounts) && Objects.equals(transactionType, that.transactionType) && Objects.equals(amount, that.amount);
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientsAccounts, transactionType, amount);
    }
}
