package modelForCraditAgencyDb;

import org.jdbi.v3.core.mapper.Nested;

import java.util.Objects;

public class ClientsAccounts {

    private Integer id;
    @Nested
    private Client client;
    @Nested
    private Account account;

    public ClientsAccounts(Client client, Account account) {
        this.client = client;
        this.account = account;
    }

    public ClientsAccounts() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "ClientsAccounts{" +
                "id=" + id +
                ", client=" + client +
                ", account=" + account +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientsAccounts that = (ClientsAccounts) o;
        return Objects.equals(id, that.id) && Objects.equals(client, that.client) && Objects.equals(account, that.account);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, client, account);
    }
}
