package modelForCraditAgencyDb;

import org.jdbi.v3.core.mapper.Nested;

import java.util.Objects;

public class Account {

    private Integer accountId;
    private String accountNumber;
    private Double balance;
    @Nested
    public CreditOffers creditOffer;

    public Account() {
    }

    public Account(String accountNumber, CreditOffers creditOffer) {
        this.accountNumber = accountNumber;
        this.creditOffer=creditOffer;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public CreditOffers getCreditOffer() {
        return creditOffer;
    }

    public void setCreditOffer(CreditOffers creditOffer) {
        this.creditOffer = creditOffer;
    }

    @Override
    public String toString() {
        return "Account{" +
                "accountId=" + accountId +
                ", accountNumber='" + accountNumber + '\'' +
                ", balance=" + balance +
                ", creditOffer=" + creditOffer +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(accountId, account.accountId) && Objects.equals(accountNumber, account.accountNumber) && Objects.equals(balance, account.balance) && Objects.equals(creditOffer, account.creditOffer);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, accountNumber, balance, creditOffer);
    }
}
