package modelForCraditAgencyDb;

import java.util.Objects;

public class CreditOffers {

    private Integer creditId;
    private Integer numberOfMonths;
    private Double yearlyInterestRate;

    public CreditOffers() {
    }

    public CreditOffers(Integer numberOfMonths, Double yearlyInterestRate) {
        this.numberOfMonths = numberOfMonths;
        this.yearlyInterestRate = yearlyInterestRate;
    }

    public Integer getCreditId() {
        return creditId;
    }

    public void setCreditId(Integer creditId) {
        this.creditId = creditId;
    }

    public Integer getNumberOfMonths() {
        return numberOfMonths;
    }

    public void setNumberOfMonths(Integer numberOfMonths) {
        this.numberOfMonths = numberOfMonths;
    }

    public Double getYearlyInterestRate() {
        return yearlyInterestRate;
    }

    public void setYearlyInterestRate(Double yearlyInterestRate) {
        this.yearlyInterestRate = yearlyInterestRate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CreditOffers that = (CreditOffers) o;
        return Objects.equals(creditId, that.creditId) && Objects.equals(numberOfMonths, that.numberOfMonths) && Objects.equals(yearlyInterestRate, that.yearlyInterestRate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(creditId, numberOfMonths, yearlyInterestRate);
    }

    @Override
    public String toString() {
        return "CreditOffers{" +
                "credit_id=" + creditId +
                ", numberOfMonths=" + numberOfMonths +
                ", yearlyInterestRate=" + yearlyInterestRate +
                '}';
    }
}
