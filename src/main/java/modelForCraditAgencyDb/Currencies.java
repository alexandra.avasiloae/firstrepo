package modelForCraditAgencyDb;

import org.jdbi.v3.core.mapper.Nested;

import java.util.Objects;

public class Currencies {

    @Nested
    private Account account;
    private String currencie, country;

    public Currencies(Account account, String currencie, String country) {
        this.account = account;
        this.currencie = currencie;
        this.country = country;
    }

    public Currencies() {
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getCurrencie() {
        return currencie;
    }

    public void setCurrencie(String currencie) {
        this.currencie = currencie;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "Currencies{" +
                "account=" + account +
                ", currencie='" + currencie + '\'' +
                ", country='" + country + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Currencies that = (Currencies) o;
        return Objects.equals(account, that.account) && Objects.equals(currencie, that.currencie) && Objects.equals(country, that.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(account, currencie, country);
    }
}
