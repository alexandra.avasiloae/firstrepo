package course2oop;

public interface GeometricObject {
    double getPerimeter();

    double getArea();
}
