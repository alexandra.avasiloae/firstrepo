package course2oop;

public class ResizableCircle implements Resizable {

    private double radius;

    public ResizableCircle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public void resize(int percent) {
        this.radius = this.radius - (double) percent / 100 * radius;
    }
}
