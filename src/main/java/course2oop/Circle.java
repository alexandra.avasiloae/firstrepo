package course2oop;

public class Circle extends Shape implements GeometricObject {

    private double radius;

    public Circle() {
        super();
        this.radius = 1;
    }

    public Circle (double radius){
        this.radius=radius;
    }
    public Circle(String color, boolean isFilled, double radius) {
        super(color, isFilled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }

    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return "Circle with radius=" +
                radius + " which is a subclass off " + super.toString();

    }
}
