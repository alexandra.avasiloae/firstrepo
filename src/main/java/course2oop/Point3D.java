package course2oop;

public class Point3D extends Point2D {
    private float z;

    public Point3D(float x, float y, float z) {
        super(x, y);
        this.z = z;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    public void setXYZ(float x, float y, float z) {
        this.setX(x);
        this.setY(y);
        this.z = z;
    }

    public float[] getXYZ() {
        float[] coordonates = new float[3];
        coordonates[0] = this.getX();
        coordonates[1] = this.getY();
        coordonates[2] = this.z;
        return coordonates;
    }

    @Override
    public String toString() {
        return "(" +
                this.getX() + ", " + this.getY() + ", " + z +
                ')';
    }
}
