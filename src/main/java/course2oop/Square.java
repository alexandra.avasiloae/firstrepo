package course2oop;

public class Square extends Rectangle {

    public Square(String color, boolean isFilled, double length) {
        super(color, isFilled, length, length);
    }

    @Override
    public void setLength(double length) {
        super.setLength(length);
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width) {
        super.setWidth(width);
        super.setLength(width);
    }

    @Override
    public String toString() {
        return "Square with " +
                "l=" + getLength();
    }
}
