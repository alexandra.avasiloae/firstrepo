package course2oop;

public class Lecturer extends Person {

    private String specialization;
    double salary;

    public Lecturer(String specialization, double salary) {
        this.specialization = specialization;
        this.salary = salary;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Lecturer: " + "Specialization - " + specialization + ", " + "Salary - " + salary;
    }
}
