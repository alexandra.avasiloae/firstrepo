package course2oop;

public class Line {

    private Point2D point2D1;
    private Point2D point2D2;

    public Line(Point2D point2D1, Point2D point2D2) {
        this.point2D1 = point2D1;
        this.point2D2 = point2D2;
    }

    public Line(float x1, float y1, float x2, float y2) {
        this.point2D1 = new Point2D(x1,y1);
        this.point2D2 = new Point2D(x2,y2);
    }

    public Point2D getPoint2D1() {
        return point2D1;
    }

    public void setPoint2D1(Point2D point2D1) {
        this.point2D1 = point2D1;
    }

    public Point2D getPoint2D2() {
        return point2D2;
    }

    public void setPoint2D2(Point2D point2D2) {
        this.point2D2 = point2D2;
    }

    public double lengthOfLine() {
        return Math.sqrt(Math.pow(point2D1.getX() - point2D2.getX(), 2) + Math.pow(point2D1.getY() - point2D2.getY(), 2));

    }

    public double[] centerOfLine(){
        double[] coordonates = new double[2];
        coordonates[0]=(point2D1.getX()+point2D2.getX())/2;
        coordonates[1]=(point2D1.getY()+point2D2.getY())/2;
        return coordonates;
    }


}
