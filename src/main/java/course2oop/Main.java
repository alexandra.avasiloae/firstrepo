package course2oop;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        //Task 1
        Point2D point2D = new Point2D(2, 0.1f);
        System.out.println(point2D.toString());
        Point3D point3D = new Point3D(1.2f, 0, -1);
        System.out.println(Arrays.toString(point3D.getXYZ()));

        //Task 2
        Person person = new Person("Alexandra", "Str. Vasile Lupu, Iasi, Romani");
        System.out.println(person.toString());

        Student student = new Student("Informatica", 3, 3000);
        System.out.println(student.toString());

        Lecturer lecturer = new Lecturer("OOP", 5000);
        System.out.println(lecturer.toString());

        //Task 3
        /*
        Shape shape = new Shape("RED", true);
        System.out.println(shape.toString());
        Shape shape2 = new Shape("GREEN", false);
        System.out.println(shape2.toString());
         */
        Circle circle = new Circle("BLUE", true, 2.5);
        System.out.println(circle.toString());
        System.out.println("Area = " + circle.getArea());
        System.out.println("Perimeter " + circle.getPerimeter());

        Rectangle rectangle = new Rectangle("ORANGE", true, 2, 3);
        System.out.println(rectangle.toString());
        System.out.println("Area = " + rectangle.getArea());
        System.out.println("Perimeter = " + rectangle.getPerimeter());

        Square square = new Square("YELLOW", false, 3);
        System.out.println(square.toString());
        System.out.println("Area = " + square.getArea());
        System.out.println("Perimeter = " + square.getPerimeter());


        //Task 5
        Point2D point2D1 = new Point2D(2, -3);
        Point2D point2D2 = new Point2D(1.5f, 0.5f);
        Line line = new Line(point2D1, point2D2);
        System.out.println("Length of the line = " + line.lengthOfLine());

        Line line1 = new Line(1,2,3,1);
        System.out.println("Center = " + Arrays.toString(line1.centerOfLine()));

        //Task 6
        MovablePoint point = new MovablePoint(2,4,1,1.5f);
        point.moveLeft();
        System.out.println(point.toString());

        MovableCircle movableCircle =  new MovableCircle(point,4);
        movableCircle.moveDown();
        System.out.println(movableCircle.getMovablePoint());

        //Task 7
        ResizableCircle resizableCircle = new ResizableCircle(4);
        resizableCircle.resize(5);
        System.out.println(resizableCircle.getRadius());



    }
}
