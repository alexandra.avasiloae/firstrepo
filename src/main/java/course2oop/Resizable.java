package course2oop;

public interface Resizable {

    void resize(int percent);
}
