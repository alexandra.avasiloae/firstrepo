package shoppingCart;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Basket {

    private List<Book> books;

    public Basket() {
        books = new LinkedList<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void clearShoppingCard() {
        books.clear();
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }

    public Double sumOfPrices() {
        double sum = 0;
        for (Book book : books) {
            sum += book.getPrice();
        }
        return sum;
    }
}
