package Exceptions;

public class NoBookFoundException extends Exception {
    public NoBookFoundException(String s) {
        super(s);
    }
}
