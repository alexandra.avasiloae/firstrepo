package Exceptions;

public class Main {

    //task 1
    public static double divide(double x, double y) throws CannotDivideBy0Exception {
        if(y==0){
            throw new CannotDivideBy0Exception();
        }
        else{
            return x/y;
        }
    }

    public static void main(String[] args) throws CannotDivideBy0Exception, NoBookFoundException {
        //Task 1
        //System.out.println(divide(3,0));
        //System.out.println(divide(5,2));

        //Task 2

        Book book1 = new Book("Pride and Prejudice ","44@345","Jane Austen",1813);
        Book book2 = new Book("Gone with the wind","00@675","Margaret Mitchell",1936);
        Book book3 = new Book("The Little Prince","22@123","Antoine de Saint-Exupéry",1945);

        BookRepository bookRepository = new BookRepository();
        bookRepository.add(book1);
        bookRepository.add(book2);
        bookRepository.add(book3);

        //bookRepository.remove(book3);
        //System.out.println(bookRepository.findById("11@123"));
        System.out.println(bookRepository.findById("00@675").toString());

        bookRepository.removeById("123");

    }
}
