package Exceptions;

import java.util.ArrayList;
import java.util.List;

public class BookRepository {

    private List<Book> books = new ArrayList<>();

    public void add(Book book) {
        this.books.add(book);
    }

    public void remove(Book book) throws NoBookFoundException {
        if (this.books.contains(book)) {
            books.remove(book);
        } else {
            throw new NoBookFoundException("This book does not exist");
        }
    }

    public Book findById(String id) throws NoBookFoundException {
        for (Book book : this.books) {
            if (book.getId().equals(id)) {
                return book;
            }
        }
        throw new NoBookFoundException("This book id does not exist!");
    }

    public Book findByName(String name) throws NoBookFoundException {
        for (Book book : this.books) {
            if (book.getTitle().equals(name)) {
                return book;
            }
        }
        throw new NoBookFoundException("This book name does not exist!");
    }

    public void removeById(String id) throws NoBookFoundException {
        boolean bookIdFound = false;
        for (Book book : this.books) {
            if (book.getId().equals(id)) {
                this.books.remove(book);
                bookIdFound = true;
            }
        }
        if (!bookIdFound) {
            throw new NoBookFoundException("This book id does not exist!");
        }
    }

    public List<Book> getBooks() {
        return books;
    }

    public void setBooks(List<Book> books) {
        this.books = books;
    }
}
