package Exceptions;

public class CannotDivideBy0Exception extends Exception {
    public CannotDivideBy0Exception() {
        super("Divide by 0!");
    }
}
