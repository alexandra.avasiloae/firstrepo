package ExtraEx;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtraEx2 {

    public static double averageOfMany(double... a) {
        int crt = 0;
        double sum = 0;
        for (double i : a) {
            crt++;
            sum += i;
        }
        return sum / crt;
    }

    public static double harmonicAverageOfMany(double... a) {
        int crt = 0;
        double sum = 0;
        for (double i : a) {
            crt++;
            sum += 1 / i;
        }
        return crt / sum;
    }

    public static Map<Character, Integer> numberOfChar(String text) {

        Map<Character, Integer> countsChars = new HashMap<>();
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            countsChars.merge(c, 1, Integer::sum);
        }
        return countsChars;
    }


    public static String transformInLink(String text) {

        String link;
        link = text.replace(' ', '-');
        link = link.replaceAll("[^a-zA-Z0-9-]", "");
        link = link.toLowerCase();
        link = link.replace('ă', 'a');
        link = link.replace('â', 'a');
        link = link.replace('î', 'i');
        link = link.replace('ț', 't');
        link = link.replace('ș', 's');
        return link;

    }

    public static void printTableOfMultiplication() {
        for (int i = 0; i <= 10; i++) {
            for (int j = 0; j <= 10; j++) {
                System.out.println(i + "*" + j + "=" + i * j);
            }
        }
    }

    public static boolean checkPassword(String password) {
        String regex = "^(?=.*[a-z])(?=."
                + "*[A-Z])(?=.*\\d)"
                + "(?=.*[-+_!@#$%^&*., ?]).+$";

        Pattern p = Pattern.compile(regex);

        Matcher m = p.matcher(password);

        return (m.matches() && password.length() >= 8);
    }

    public static int byteToIn(String binary) {
        return Integer.parseInt(binary, 2);
    }

    public static int wordCount(String text) {
        String[] words = text.split("\\s");
        return words.length;
    }

    public static double monthlyPayment(double lending, int numberOfYears) {
        return (lending / numberOfYears) / 12;
    }

    public static float balanceToFloat(String balance) {
        balance = balance.replace("€", "");
        return Float.parseFloat(balance);
    }

    public static void main(String[] args) {

        //Ex 1
        System.out.println(averageOfMany(-2.1, 3, -0.88, 4));

        //Ex2
        System.out.println(harmonicAverageOfMany(-2.1, 4, -0.8));

        //Ex 3
        System.out.println(numberOfChar("anamaria"));

        //Ex 4
        System.out.println(transformInLink("Hp Elitebook dv7886 laptop mândru 50% reducere"));

        //Ex 5
        printTableOfMultiplication();

        //Ex 6
        System.out.println(checkPassword("Alexandra2000!"));

        //Ex 7
        System.out.println(byteToIn("100"));

        //Ex 8
        System.out.println(wordCount("Ana are mere!"));

        //Ex 9
        System.out.println(monthlyPayment(1234, 2));

        //Ex 10
        System.out.println(balanceToFloat("123.4 €"));


    }
}
