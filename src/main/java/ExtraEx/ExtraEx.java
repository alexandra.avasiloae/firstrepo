package ExtraEx;

import java.util.Random;

public class ExtraEx {

    /**
     * average of 3 floats
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public static float averageOf3(float a, float b, float c) {
        return (a + b + c) / 3;
    }


    /**
     * harmonic mean of 3 floats
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public static float harmonicMeanOf3(float a, float b, float c) {
        return 3 / (1 / a + 1 / b + 1 / c);
    }

    /**
     * this method returns real solutions for the 2nd degree equation
     * the parameters are the coefficient of equation
     *
     * @param a
     * @param b
     * @param c
     * @return
     */
    public static double[] solutionEquation(double a, double b, double c) {
        double delta = b * b - 4 * a * c;
        double[] solutions = new double[2];
        if (delta >= 0) {
            double x1 = (-b - Math.sqrt(delta)) / (2 * a * c);
            solutions[0] = x1;
            double x2 = (-b + Math.sqrt(delta)) / (2 * a * c);
            solutions[1] = x2;
        }
        return solutions;
    }

    /**
     * this method simulates tossing a coin and return 0 ori 1 pseudo-randomly
     *
     * @return
     */
    public static boolean tossingACoin() {
        Random random = new Random();
        return random.nextBoolean();

    }

    public static void rectangleOf(int n, int m) {
        StringBuilder line = new StringBuilder();
        for (int i = 0; i < n; i++) {
            line.append("#");

        }
        for (int j = 0; j < m; j++) {
            System.out.println(line);
        }
    }

    public static void christmasTree(int base) {

        int i = 0;
        while (i < base) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < (base - i) / 2; j++) {
                line.append(" ");
            }
            for (int j = 0; j < i + 1; j++) {
                line.append("#");
            }
            for (int j = 0; j < (base - i) / 2; j++) {
                line.append(" ");
            }

            System.out.println(line);
            i += 2;
        }
    }

    public static void christmasTreeWithBackground(int base) {

        int i = 0;
        while (i < base - 1) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < (base - i) / 2 - 1; j++) {
                line.append("^");
            }
            line.append(" ");
            for (int j = 0; j < i + 1; j++) {
                line.append("#");
            }
            line.append(" ");
            for (int j = 0; j < (base - i) / 2 - 1; j++) {
                line.append("^");
            }

            System.out.println(line);
            i += 2;
        }
        StringBuilder line = new StringBuilder();
        for (int j = 0; j < base; j++) {
            line.append("^");
        }
    }


    public static void triangleOf(int base) {
        int i = 0;
        while (i < base) {
            StringBuilder line = new StringBuilder();
            for (int j = 0; j < i + 1; j++) {
                line.append("#");

            }
            System.out.println(line);
            i++;
        }
    }


    public static void main(String[] args) {

        //Ex 1
        System.out.println(averageOf3(3.2f, 4f, -6.2f));

        //Ex 2
        System.out.println(harmonicMeanOf3(3.1f, -0.9f, 4));

        //Ex 3

        double[] sol = solutionEquation(1, -2, 1);
        System.out.println("Solutiile ecuatiei: ");
        for (int i = 0; i < 2; i++) {
            System.out.println(sol[i]);
        }

        //Ex 4
        System.out.println(tossingACoin());


        //Ex 5
        rectangleOf(5, 5);

        //Ex 6
        System.out.println("\n");
        triangleOf(4);

        //Ex 7
        System.out.println("\n");
        christmasTree(7);

        //Ex 8
        System.out.println("\n");
        christmasTreeWithBackground(9);
    }
}
