package threads;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Task8 {

    public static void main(String[] args) {

        ListProvider<Integer> list = new ListProvider<>();
        Thread t1 = new Thread(() -> {

            for (int i = 0; i < 10; i++) {
                list.addElem(new Random().nextInt(100));
            }
        });
        t1.start();

        Thread t2 = new Thread(() -> {

            for (int i = 0; i < 10; i++) {
                list.addElem(new Random().nextInt(100));
            }
        });
        t2.start();

    }
}

class ListProvider<T> {

    private final List<T> listOfRandoms;

    public ListProvider() {

        listOfRandoms = new ArrayList<>();
    }

    public synchronized void addElem(T t) {

        listOfRandoms.add(t);
        System.out.println(Thread.currentThread().getName());
        System.out.println(listOfRandoms);
    }

}
