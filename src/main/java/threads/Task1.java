package threads;

public class Task1 {

    public synchronized void evenNumbersInInterval(int x, int y) {

        for (int i = x; i <= y; i++) {
            if (i % 2 == 0) {
                System.out.println("Thread id = "+ Thread.currentThread().getId() + " - Even number = " + i);
            }
        }
    }

    public static void main(String[] args) {

        final Thread t1 = new Thread(new findEvenNumbersInInterval(1000, 2000));
        final Thread t2 = new Thread(new findEvenNumbersInInterval(14300, 17800));

        t1.start();
        t2.start();
    }
}

class findEvenNumbersInInterval implements Runnable {

    public Task1 task = new Task1();
    public int x, y;

    public findEvenNumbersInInterval(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public void run() {
        task.evenNumbersInInterval(x, y);
    }
}
