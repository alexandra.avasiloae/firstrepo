package threads;

import java.util.ArrayList;
import java.util.List;

public class Task5<T> {

    public static void main(String[] args) {

        List<String> list = new ArrayList<>();
        list.add("a1");
        list.add("a2");
        list.add("a3");
        list.add("a4");
        list.add("a5");
        list.add("a6");
        list.add("a7");
        list.add("a8");
        list.add("a9");
        list.add("a10");

        ListIterator<String> listIterator = new ListIterator<>(list);

        Thread t1 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(listIterator.next());
                }
            }
        });
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(5);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(listIterator.prev());
                }
            }
        });
        t1.start();
        t2.start();
    }
}

class ListIterator<T> {

    private List<T> list;
    private Integer index = -1;

    public ListIterator(List<T> list) {
        this.list = list;
    }

    public T next() {
        synchronized (index) {
            if (index > list.size() - 1) {
                return null;
            } else {
                index++;
                return list.get(index);
            }
        }
    }

    public synchronized T prev() {
        synchronized (index) {
            if (index < 1) {
                return null;
            } else {
                index--;
                return list.get(index);
            }
        }
    }
}