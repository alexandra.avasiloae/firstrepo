package threads;

public class Task4 {

    public static void main(String[] args) {
        final InternetService internetService = new InternetService();
        final Thread chargeThread = new Thread(new ChargeThread(internetService, 1500));
        final Thread depositThreadA = new Thread(new Deposit(internetService, 1000));
        final Thread depositThreadB = new Thread(new Deposit(internetService, 700));

        chargeThread.start();
        depositThreadA.start();
        depositThreadB.start();
    }
}

class InternetService {
    private int availableAmount;

    public InternetService() {
        this.availableAmount = 0;
    }

    public int getAvailableAmount() {
        return availableAmount;
    }

    synchronized void charge(int amount) {
        System.out.println("Trying to charge " + amount);
        while (availableAmount < amount) {
            System.out.println("Insufficient funds!");
            try {
                wait();
            } catch (InterruptedException e) {
                System.err.println("Oops");
            }
        }
        System.out.println("Charge successful!");
        availableAmount -= amount;
    }

    synchronized void deposit(int amountToDeposit) {
        System.out.println("Depositing " + amountToDeposit);
        availableAmount += amountToDeposit;
        notify();
    }
}

class ChargeThread implements Runnable {

    private InternetService internetService = new InternetService();
    private final int amount;

    ChargeThread(final InternetService internetService, int amount) {
        this.internetService = internetService;
        this.amount = amount;
    }


    @Override
    public void run() {
        internetService.charge(amount);
        System.out.println("Balance = " + internetService.getAvailableAmount());
    }
}

class Deposit implements Runnable {
    private final InternetService internetService;
    private final int amount;

    Deposit(final InternetService internetService, int amount) {
        this.internetService = internetService;
        this.amount = amount;
    }

    @Override
    public void run() {
        internetService.deposit(amount);
        System.out.println("Balance = " + internetService.getAvailableAmount());
    }
}
