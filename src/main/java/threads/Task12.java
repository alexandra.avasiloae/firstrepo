package threads;

import java.util.Random;

public class Task12 {

    private static Integer balance = 1000;

    public static boolean withdraw(int amount) {
        synchronized (balance) {
            int diff = balance - amount;
            if (diff >= 0) {
                balance -= amount;
                return true;
            } else {
                return false;
            }
        }
    }

    public static void main(String[] args) {

        Thread t1 = new Thread(() -> {
            while (withdraw(new Random().nextInt(200))) {
                System.out.println(Thread.currentThread().getName() + " - " + balance);
            }
        });

        Thread t2 = new Thread(() -> {
            while (withdraw(new Random().nextInt(200))) {
                System.out.println(Thread.currentThread().getName() + " - " + balance);
            }
        });

        Thread t3 = new Thread(() -> {
            while (withdraw(new Random().nextInt(200))) {
                System.out.println(Thread.currentThread().getName() + " - " + balance);
            }
        });
        t1.start();
        t2.start();
        t3.start();
    }
}

