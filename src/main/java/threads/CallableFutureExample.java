package threads;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

class RunnableClass implements Runnable {

    @Override
    public void run() {
        System.out.println("Run method here!");
    }
}

class RunnableClass1 implements Runnable {

    @Override
    public void run() {
        System.out.println("Run method here! x2");
    }
}

public class CallableFutureExample {

    public static void main(String[] args) {
        ExecutorService executorService = Executors.newSingleThreadExecutor(); // creating an ExecutorService with a single-threaded pool
        Future<String> result = executorService.submit(() -> "I am result of callable!"); // Callable implementation using lambda
        try {
            System.out.println("Prinint result of the future: " + result.get());
        } catch (InterruptedException | ExecutionException e) {
            System.err.println("Oops");
        }
        executorService.shutdown(); // remember to close the ExecutorService manually

        ExecutorService executorService1 = Executors.newSingleThreadExecutor(); // creating an ExecutorService with a single-threaded pool
        RunnableClass runnableClass = new RunnableClass();
        executorService1.submit(runnableClass);

        executorService1.shutdown();

    }
}