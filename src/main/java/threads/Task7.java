package threads;

import java.util.Random;

public class Task7 {

    public static void printRandoms(int valuesGenerated, long delay) {
        for (int i = 0; i < valuesGenerated; i++) {
            System.out.println(new Random().nextInt(10));
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Thread t1 = new Thread(() -> printRandoms(10, 500));

        Thread t2 = new Thread(() -> printRandoms(5, 100));

        Thread t3 = new Thread(() -> printRandoms(7, 250));

        t1.start();
        t1.join();
        t2.start();
        t2.join();
        t3.start();
        t3.join();
    }


}
