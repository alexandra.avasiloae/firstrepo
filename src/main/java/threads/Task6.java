package threads;

import java.util.ArrayList;
import java.util.List;

public class Task6 {

    public static void main(String[] args) {

        //1
        new FirstNPrimesThread().start();


        //2
        Thread t2 = new Thread(new Runnable() {
            @Override
            public void run() {

                System.out.println("Thread id = " + Thread.currentThread().getId());
                FirstPrimes.nThPrime(10);

            }
        });
        t2.start();


        //3
        Thread t3 = new Thread(() -> {

            System.out.println("Thread id = " + Thread.currentThread().getId());
            FirstPrimes.nThPrime(10);
        });
        t3.start();

    }
}

class FirstPrimes {

    private int n;

    public static void nThPrime(int n) {

        int max = 10000;
        List<Integer> primes = new ArrayList<>();
        for (int i = 2; i < max; i++) {
            if (isPrime(i)) {
                primes.add(i);
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.println(primes.get(i));
        }
    }

    public static boolean isPrime(double value) {

        if (value <= 1)
            return false;
        if (value % 2 == 0 && value != 2) {
            return false;
        }
        for (int i = 3; i < value / 2; i++)
            if (value % i == 0)
                return false;

        return true;
    }
}

class FirstNPrimesThread extends Thread {

    FirstPrimes firstPrimes = new FirstPrimes();

    @Override
    public void run() {

        System.out.println("Thread id = " + Thread.currentThread().getId());
        FirstPrimes.nThPrime(10);
    }
}
