package threads;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

//methods from ArrayBlockingQue are synchronized
public class Task9 {

    public static void consumeFromQueue(ArrayBlockingQueue queue){

        while (!queue.isEmpty()) {
            try {
                System.out.println(Thread.currentThread().getName());
                queue.take();
                System.out.println(queue);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {

        ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<>(10, true, Arrays.asList(1, 2, 3, -4, 4, 5, 7, -9, 1, 1));
        Thread t1 = new Thread(() -> consumeFromQueue(queue));
        Thread t2 = new Thread(() -> consumeFromQueue(queue));
        Thread t3 = new Thread(() -> consumeFromQueue(queue));
        t1.start();
        t2.start();
        t3.start();
    }
}
