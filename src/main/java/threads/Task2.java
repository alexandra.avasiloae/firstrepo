package threads;

public class Task2 {
    public static void main(String[] args) {

        Car car1 = new Car("Ford", "TD1");
        Car car2 = new Car("Volvo", "4x4");
        Bridge bridge = new Bridge();

        Thread t1 = new Thread(() -> {
            try {
                bridge.driveThrough(car1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        Thread t2 = new Thread(() -> {
            try {
                bridge.driveThrough(car2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        t1.start();
        t2.start();
    }
}

class Car {
    private String name, type;

    public Car(String name, String type) {
        this.name = name;
        this.type = type;
    }

    public void onBridge() {
        System.out.println(name + " is on the bridge");
    }
}

class Bridge {
    public synchronized void driveThrough(Car car) throws InterruptedException {
        System.out.println("Thread id = " + Thread.currentThread().getId());
        car.onBridge();
        Thread.sleep(5);
    }
}