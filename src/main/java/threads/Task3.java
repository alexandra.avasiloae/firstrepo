package threads;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task3 {

    public static void main(String[] args) {
        SortedMethods sortedMethods = new SortedMethods();
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        int[] arr = new int[10];
        int[] arr2 = new int[10];

        for (int i = 0; i < 10; i++) {
            int random = new Random().nextInt(10);
            arr[i] = random;
            arr2[i] = random;

        }

        List<Callable<String>> tasks = Arrays.asList(
                () -> {
                    System.out.println("Thread: " + Thread.currentThread().getName());
                    System.out.println("Bubble sort...");
                    sortedMethods.bubbleSort(arr);
                    return "Bubble sort finished!";
                },
                () -> {
                    System.out.println("Thread: " + Thread.currentThread().getName());
                    System.out.println("Merge sort...");
                    sortedMethods.mergeSort(arr2, 0, arr2.length - 1);
                    return "Merge sort finished!";
                }
        );
        try {
            String firstResult = executorService.invokeAny(tasks);
            System.out.println("FIRST RESULT: " + firstResult);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        executorService.shutdown();
    }
}

class SortedMethods {

    public void bubbleSort(int[] arr) {

        int n = arr.length;
        int aux = 0;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (arr[j - 1] > arr[j]) {
                    aux = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = aux;
                }
            }
            System.out.println(Thread.currentThread().getId() + " - " + Arrays.toString(arr));
        }
    }

    public void mergeSort(int[] arr, int l, int r) {
        if (l < r) {
            int m = l + (r - l) / 2;
            mergeSort(arr, l, m);
            mergeSort(arr, m + 1, r);
            merge(arr, l, m, r);
        }
    }

    void merge(int[] arr, int l, int m, int r) {
        int n1 = m - l + 1;
        int n2 = r - m;

        int[] L = new int[n1];
        int[] R = new int[n2];

        System.arraycopy(arr, l, L, 0, n1);
        for (int j = 0; j < n2; ++j)
            R[j] = arr[m + 1 + j];

        int i = 0, j = 0;

        int k = l;
        while (i < n1 && j < n2) {
            if (L[i] <= R[j]) {
                arr[k] = L[i];
                i++;
            } else {
                arr[k] = R[j];
                j++;
            }
            k++;
        }

        while (i < n1) {
            arr[k] = L[i];
            i++;
            k++;
        }

        while (j < n2) {
            arr[k] = R[j];
            j++;
            k++;
        }
    }
}
