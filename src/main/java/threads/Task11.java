package threads;

import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;

//methods from ArrayBlockingQue are synchronized
public class Task11 {

    public static void countPrimeFactorsFromQueueElements(ArrayBlockingQueue<Integer> queue){

        while (!queue.isEmpty()) {
            int value = queue.poll();
            System.out.println(Thread.currentThread().getName() + " - " + value + " - number of prime factors = " + countPrimeFactors(value));
        }
    }

    public static int countPrimeFactors(int n) {

        int crt = 0;
        while (n % 2 == 0) {
            crt++;
            n /= 2;
        }

        for (int i = 3; i <= Math.sqrt(n); i += 2) {
            while (n % i == 0) {
                crt++;
                n /= i;
            }
        }

        if (n > 2)
            crt++;
        return crt;
    }

    public static void main(String[] args) {

        ArrayBlockingQueue<Integer> queue = new ArrayBlockingQueue<>(5, true, Arrays.asList(2, 3, 4, 40, 15));
        Thread t1 = new Thread(() -> countPrimeFactorsFromQueueElements(queue));
        Thread t2 = new Thread(() -> countPrimeFactorsFromQueueElements(queue));
        Thread t3 = new Thread(() -> countPrimeFactorsFromQueueElements(queue));
        t1.start();
        t2.start();
        t3.start();
    }
}
