package threads;

public class ThreadsExample {
    public static void main(String[] args) {
        new HelloWorldThread().start();
        System.out.println(Thread.currentThread().getId());
    }
}

class HelloWorldThread extends Thread {
    @Override
    public void run() {
        System.out.println("Hello World from another Thread");
        System.out.println(Thread.currentThread().getId());
    }
}
