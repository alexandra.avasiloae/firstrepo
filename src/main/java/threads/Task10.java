package threads;

public class Task10 {

    public static void main(String[] args) {

        Number number = new Number(10);
        Thread t1 = new Thread(() -> {
            while (true) {
                number.increase();
                System.out.println(Thread.currentThread().getName() + " - number = " + number.getNumber());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t2 = new Thread(() -> {
            while (true) {
                number.increase();
                System.out.println(Thread.currentThread().getName() + " - number = " + number.getNumber());
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        t1.start();
        t2.start();
    }
}

class Number {
    private int number;

    public Number(int number) {
        this.number = number;
    }

    public synchronized void increase() {
        number++;
    }

    public int getNumber() {
        return number;
    }
}
