package subunitSaving;

public class BaseAccount extends Account {

    public BaseAccount(String accountID, double balance, String type) {
        super(accountID, balance, type);
    }

    @Override
    public String getAccountID() {
        return null;
    }

    @Override
    public void setAccountID(String accountID) {

    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String getType() {
        return this.type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void add(double amount) {
        this.balance += amount;
    }

}
