package subunitSaving;

public class Savings extends Account {

    public Savings(String accountID, String type) {
        super(accountID, type);
    }

    @Override
    public String getAccountID() {
        return accountID;
    }

    @Override
    public void setAccountID(String accountID) {
        this.accountID = accountID;
    }

    @Override
    public double getBalance() {
        return this.balance;
    }

    @Override
    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public void add(double amount) {
        this.balance += amount;
    }

}
