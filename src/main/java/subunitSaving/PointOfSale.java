package subunitSaving;

import javax.inject.Inject;
import java.math.BigDecimal;

public class PointOfSale implements Payments {

    private User user;
    private Double amount;

    public User getUser() {
        return user;
    }

    public PointOfSale() {
    }

    @Inject
    public PointOfSale(User user, @Amount Double amount) {
        this.user = user;
        this.amount = amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getAmount() {
        return amount;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void pay() {
        if (user.getBaseAccount().getBalance() >= amount) {
            this.user.getBaseAccount().setBalance(user.getBaseAccount().getBalance() - amount);
            BigDecimal balance = BigDecimal.valueOf(user.getSavingsAccount().getBalance());
            BigDecimal amountRound = BigDecimal.valueOf(Math.ceil(amount));
            BigDecimal saved = amountRound.subtract(BigDecimal.valueOf(amount));
            this.user.getSavingsAccount().setBalance(this.user.getSavingsAccount().getBalance() + saved.doubleValue());
            System.out.println("User base account balance = " + user.getBaseAccount().getBalance());
            System.out.println("User saving account = " + user.getSavingsAccount().getBalance());
        } else {
            System.out.println("Insufficient funds");
        }
    }
}
