package subunitSaving;

public class User {

    private String name;
    private int ID;
    private Savings savingsAccount;
    private BaseAccount baseAccount;

    public User(String name, int ID) {
        this.name = name;
        this.ID = ID;
    }

    public User(String name, int ID, Savings savingsAccount, BaseAccount baseAccount) {
        this.name = name;
        this.ID = ID;
        this.savingsAccount = savingsAccount;
        this.baseAccount = baseAccount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getID() {
        return ID;
    }

    public Savings getSavingsAccount() {
        return savingsAccount;
    }

    public void setSavingsAccount(Savings savingsAccount) {
        this.savingsAccount = savingsAccount;
    }

    public BaseAccount getBaseAccount() {
        return baseAccount;
    }

    public void setBaseAccount(BaseAccount baseAccount) {
        this.baseAccount = baseAccount;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void createBaseAccount(String accountID, double balance, String type) {
        this.baseAccount = new BaseAccount(accountID, balance, type);
    }

    public void createSavings(String accountID, String type) {
        this.savingsAccount = new Savings(accountID, type);
    }



}
