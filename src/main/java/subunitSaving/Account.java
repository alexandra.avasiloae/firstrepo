package subunitSaving;

public abstract class Account {

    protected String accountID;
    protected double balance;
    protected String type; //lei, euro, etc

    public Account(String accountID, String type) {
        this.accountID = accountID;
        this.type = type;
    }

    public Account(String accountID, double balance, String type) {
        this.accountID = accountID;
        this.balance = balance;
        this.type = type;
    }

    public abstract String getAccountID();

    public abstract void setAccountID(String accountID);

    public abstract double getBalance();

    public abstract void setBalance(double balance);

    public abstract String getType();

    public abstract void setType(String type);

    public abstract void add(double amount);

}
