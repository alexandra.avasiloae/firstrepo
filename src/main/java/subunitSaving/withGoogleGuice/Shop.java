package subunitSaving.withGoogleGuice;

import com.google.inject.Inject;
import subunitSaving.Payments;
import subunitSaving.PointOfSalePay;

public class Shop {

    @Inject @PointOfSalePay
    private Payments pointOfSalePay;

    public void payWithPointOfSale() {
       pointOfSalePay.pay();
    }
}
