package subunitSaving.withGoogleGuice;

import com.google.inject.AbstractModule;
import subunitSaving.*;

public class PayModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(Double.class)
                .annotatedWith(Amount.class)
                .toInstance(20.5);

        bind(Payments.class)
                .annotatedWith(PointOfSalePay.class)
                .to(PointOfSale.class);

        bind(User.class)
                .toInstance(new User("Alexandra",1111, new Savings("RO3456","RON"), new BaseAccount("RO1234", 1000D, "RON")));
    }
}
