package subunitSaving;

public class Main {

    public static void main(String[] args) {
        User user = new User("Alexandra", 1111);
        user.createBaseAccount("RO1234", 1000, "RON");
        user.createSavings("RO3456", "RON");

        PointOfSale pointOfSale = new PointOfSale();
        pointOfSale.setUser(user);

        pointOfSale.setAmount(20.5);
        pointOfSale.pay();
        System.out.println("User base account balance = " + user.getBaseAccount().getBalance());
        System.out.println("User saving account = " + user.getSavingsAccount().getBalance());
    }
}
