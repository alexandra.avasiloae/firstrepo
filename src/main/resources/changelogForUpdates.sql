--liquibase formatted sql
--changeset alexandra:2

ALTER TABLE accounts
DROP FOREIGN KEY id_client_fk;

--changeset alexandra:3
ALTER TABLE accounts
DROP Column client_id;

--changeset alexandra:100 labels:v1
DROP TABLE IF EXISTS `clients_accounts`;
create table clients_accounts (
                               id int not null primary key,
                               id_client int not null,
                               id_account int not null,
                               CONSTRAINT `id_client_fk` FOREIGN KEY (`id_client`) REFERENCES `clients` (`client_id`),
                               CONSTRAINT `id_account_fk` FOREIGN KEY (`id_account`) REFERENCES `accounts` (`account_id`)
);

--changeset alexandra:101 labels:v2

DROP TABLE IF EXISTS `transactions`;
create table transactions (
                                  transaction_id int not null primary key,
                                  transaction_type varchar(25) NOT NULL CHECK (transaction_type IN ('deposit','withdraw')),
                                  CONSTRAINT `transaction_id_fk` FOREIGN KEY (`transaction_id`) REFERENCES `clients_accounts` (`id`)
);

--changeset alexandra:105 labels:v3
ALTER TABLE `transactions`
    ADD COLUMN amount decimal(10,2) NOT NULL;