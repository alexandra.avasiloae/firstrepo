import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import usingtTDDapproach.Email;

public class EmailAddressTest {

    static Email email;

    @BeforeAll
    static void setUp(){
        email = new Email();
    }

    @Test
    public void validateEmailAddressTest1() {

        String emailAddress1 = "alexandra.avasiloae@mambu.com";

        boolean validateEmailExpectation1 = email.validateEmail(emailAddress1);

        Assertions.assertTrue(validateEmailExpectation1);


    }

    @Test
    public void validateEmailAddressTest2() {

        String emailAddress2 = "alexandra_avasiloae@mambu.com";

        boolean validateEmailExpectation2 = email.validateEmail(emailAddress2);

        Assertions.assertTrue(validateEmailExpectation2);

    }

    @Test
    public void validateEmailAddressTest3() {

        String emailAddress3 = "alexandra.avasiloae@mambu23.com";

        boolean validateEmailExpectation3 = email.validateEmail(emailAddress3);

        Assertions.assertTrue(validateEmailExpectation3);

    }

    @Test
    public void validateEmailAddressTest4() {

        String emailAddress4 = "alexandra_2avasiloae@mambu.com";

        boolean validateEmailExpectation4 = email.validateEmail(emailAddress4);

        Assertions.assertTrue(validateEmailExpectation4);


    }
}
