import com.beust.ah.A;
import org.assertj.core.api.Assertions;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import testsTasks.Calculator;


public class CalculatorTest {

    static Calculator calculator;

    @BeforeAll
    static void setUp() {
        calculator = new Calculator();
    }


    @Test
    public void testAdd() {

        //When
        double result1 = calculator.addition(2.1, 3.3);
        double result2 = calculator.addition(2, 3.3);
        double result3 = calculator.addition(2.08, 3);
        double result4 = calculator.addition(10000, 34567);

        //Then
        /*
        Assertions.assertEquals(5.4,result1);
        Assertions.assertEquals(5.3,result2);
        Assertions.assertEquals(5.08,result3);
        Assertions.assertEquals(44567,result4);
        */
        Assertions.assertThat(result1).isEqualTo(5.4);
        Assertions.assertThat(result2).isEqualTo(5.3);
        Assertions.assertThat(result3).isEqualTo(5.08);
        Assertions.assertThat(result4).isEqualTo(44567);
    }

    @Test
    public void testSubtraction() {

        double result = calculator.subtraction(1, 0.005);

        //Then
        //Assertions.assertEquals(0.995,result);
        Assertions.assertThat(result).isEqualTo(0.995);
    }

    @Test
    public void testMultiplication() {

        double result = calculator.multiplication(0.05, 2);

        //Assertions.assertEquals(0.1,result);
        Assertions.assertThat(result).isEqualTo(0.1);

    }

    @Test
    public void testDivision() {

        double result = calculator.multiplication(2, 0.01);

        //Assertions.assertEquals(200,result);
        Assertions.assertThat(result).isEqualTo(200);

    }
}
