package restRequestTests;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

public class RestAssuredTest {

    @BeforeAll
    public static void setUp() {
        RestAssured.baseURI = "https://reqres.in";
    }

    @Test
    public void checkIfUserExists() {
        given()
                .when()
                .get("/api/users/2")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkIfPageOfUsersExists() {
        given()
                .when()
                .get("/api/users?page=2")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkUserNotFound() {
        given()
                .when()
                .get("/api/users/23")
                .then()
                .statusCode(404);
    }

    @Test
    public void checkResourceList() {
        given()
                .when()
                .get("/api/unknown")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkResourceList2() {
        given()
                .when()
                .get("/api/unknown")
                .then()
                .body("total_pages", equalTo(2));
    }

    @Test
    public void checkSingleResourceStatusCode() {
        given()
                .when()
                .get("/api/unknown/2")
                .then()
                .statusCode(200);
    }


    @Test
    public void checkSingleResourceBody() {
        given()
                .when()
                .get("/api/unknown/2")
                .then()
                .body("data.id", equalTo(2))
                .body("data.name", containsString("fuchsia rose"));
    }

    @Test
    public void checkSingleResourceNotFound() {
        given()
                .when()
                .get("/api/unknown/23")
                .then()
                .statusCode(404);
    }

    @Test
    public void checkIfUserCanBeCreated() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("name", "Tom");
        jsonAsMap.put("job", "Trainer");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/users")
                .then()
                .statusCode(201);
    }

    @Test
    public void checkUserCreated() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("name", "morpheus");
        jsonAsMap.put("job", "leader");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/users")
                .then()
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("leader"));
    }

    @Test
    public void checkUserUpdateStatusCode() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("name", "Alexandra");
        jsonAsMap.put("job", "Internship");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .put("/api/users/2")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkUserUpdateStatusCode2() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("name", "Alexandra");
        jsonAsMap.put("job", "Internship");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .patch("/api/users/2")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkUserUpdated() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("name", "Alexandra");
        jsonAsMap.put("job", "Internship");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .put("/api/users/2")
                .then()
                .body("name", equalTo("Alexandra"))
                .body("job", equalTo("Internship"));
    }

    @Test
    public void checkDeleteUser() {
        given()
                .when()
                .delete("/api/users/2")
                .then()
                .statusCode(204);
    }

    @Test
    public void checkUserRegisterStatusCode() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "eve.holt@reqres.in");
        jsonAsMap.put("password", "pistol");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/register")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkUserRegister() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "eve.holt@reqres.in");
        jsonAsMap.put("password", "pistol");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/register")
                .then()
                .body("id", equalTo(4))
                .body("token", equalTo("QpwL5tke4Pnpja7X4"));
    }

    @Test
    public void checkUserRegisterUnsuccessfulStatusCode() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "sydney@fife");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/register")
                .then()
                .statusCode(400);
    }

    @Test
    public void checkUserRegisterUnsuccessful() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "sydney@fife");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/register")
                .then()
                .body("error", equalTo("Missing password"));
    }

    @Test
    public void checkUserLoginSuccessfulStatusCode() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "eve.holt@reqres.in");
        jsonAsMap.put("password", "cityslicka");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/register")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkUserLoginSuccessful() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "eve.holt@reqres.in");
        jsonAsMap.put("password", "cityslicka");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/register")
                .then()
                .body("token", equalTo("QpwL5tke4Pnpja7X4"));
    }

    @Test
    public void checkUserLoginUnsuccessfulStatusCode() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "peter@klaven");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/login")
                .then()
                .statusCode(400);
    }

    @Test
    public void checkUserLoginUnsuccessful() {
        Map<String, Object> jsonAsMap = new HashMap<>();

        jsonAsMap.put("email", "peter@klaven");

        given()
                .when()
                .contentType("application/json")
                .body(jsonAsMap)
                .post("/api/login")
                .then()
                .body("error", equalTo("Missing password"));
    }
}
