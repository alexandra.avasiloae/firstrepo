package restRequestTests;

import io.restassured.RestAssured;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class RestAssuredTest3 {

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://petstore.swagger.io";
    }

    @Test
    public void checkOrderSuccessfulStatusCode() {
        String payload = "{\n" +
                "  \"id\": 9,\n" +
                "  \"petId\": 48,\n" +
                "  \"quantity\": 1,\n" +
                "  \"shipDate\": \"2020-07-11T15:55:47.053Z\",\n" +
                "  \"status\": \"placed\",\n" +
                "  \"complete\": true\n" +
                "}";

        given()
                .when()
                .contentType("application/json")
                .body(payload)
                .post("/v2/store/order")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkOrderUnssuccesfulStatusCode() {
        String payload = "invalid JSON";

        given()
                .when()
                .contentType("application/json")
                .body(payload)
                .put("/v2/pet")
                .then()
                .statusCode(400);
    }

    @Test
    public void checkIfExistingOrderStatusCode() {
        given()
                .when()
                .get("/v2/store/order/9")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkIfExistingOrderResponseBody() {
        given()
                .when()
                .get("/v2/store/order/9")
                .then()
                .body("petId", equalTo(48))
                .body("status", equalTo("placed"));
    }
}
