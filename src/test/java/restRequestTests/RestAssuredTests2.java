package restRequestTests;

import io.restassured.RestAssured;
import io.restassured.matcher.ResponseAwareMatcher;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Collections;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class RestAssuredTests2 {

    @BeforeAll
    public static void setup() {
        RestAssured.baseURI = "https://petstore.swagger.io";
    }

    @Test
    public void checkCreateNewPetStatusCode() {
        String json = "{\n" +
                "  \"id\": 555,\n" +
                "  \"category\": {\n" +
                "    \"id\": 223,\n" +
                "    \"name\": \"Hamsters\"\n" +
                "  },\n" +
                "  \"name\": \"Alex\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 546,\n" +
                "      \"name\": \"to be adopted\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";

        given()
                .when()
                .contentType("application/json")
                .body(json)
                .post("/v2/pet")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkCreateNewPetBodyResponse() {
        String json = "{\n" +
                "  \"id\": 555,\n" +
                "  \"category\": {\n" +
                "    \"id\": 223,\n" +
                "    \"name\": \"Hamsters\"\n" +
                "  },\n" +
                "  \"name\": \"Alex\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 546,\n" +
                "      \"name\": \"to be adopted\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"available\"\n" +
                "}";

        ResponseAwareMatcher<Response> equalTo;
        given()
                .when()
                .contentType("application/json")
                .body(json)
                .post("/v2/pet")
                .then()
                .body("id", equalTo(555))
                .body("category.id", equalTo(223))
                .body("tags.id", equalTo(Collections.singletonList(546)))
                .body("photoUrls", equalTo(Collections.singletonList("string")));
    }

    @Test
    public void checkUpdatePetStatusCode() {
        String payload = "{\n" +
                "  \"id\": 555,\n" +
                "  \"category\": {\n" +
                "    \"id\": 223,\n" +
                "    \"name\": \"Hamsters\"\n" +
                "  },\n" +
                "  \"name\": \"Alex\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 111,\n" +
                "      \"name\": \"adopted\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"sold\"\n" +
                "}";

        given()
                .when()
                .contentType("application/json")
                .body(payload)
                .put("/v2/pet")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkUpdatePetBodyResponse() {
        String payload = "{\n" +
                "  \"id\": 555,\n" +
                "  \"category\": {\n" +
                "    \"id\": 223,\n" +
                "    \"name\": \"Hamsters\"\n" +
                "  },\n" +
                "  \"name\": \"Alex\",\n" +
                "  \"photoUrls\": [\n" +
                "    \"string\"\n" +
                "  ],\n" +
                "  \"tags\": [\n" +
                "    {\n" +
                "      \"id\": 111,\n" +
                "      \"name\": \"adopted\"\n" +
                "    }\n" +
                "  ],\n" +
                "  \"status\": \"sold\"\n" +
                "}";

        given()
                .when()
                .contentType("application/json")
                .body(payload)
                .put("/v2/pet")
                .then()
                .body("category.name", equalTo("Hamsters"));
    }

    @Test
    public void checkGetPetStatusCode() {
        given()
                .when()
                .get("/v2/pet/555")
                .then()
                .statusCode(200);
    }

    @Test
    public void checkGetPetResponseBody() {
        given()
                .when()
                .get("/v2/pet/555")
                .then()
                .body("name", equalTo("Alex"))
                .body("category.name", equalTo("Hamsters"));
    }
}