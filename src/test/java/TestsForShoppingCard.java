import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import shoppingCart.Basket;
import shoppingCart.Book;

public class TestsForShoppingCard {

    public static Book book1 = new Book("Gone with the wind", "Margaret Mitchell", 20.6);
    public static Book book2 = new Book("Ion", "Liviu Rebreanu", 15.35);
    public static Basket basket = new Basket();

    @BeforeAll
    public static void setUp() {
        basket.addBook(book1);
        basket.addBook(book2);
    }

    @Test
    public void testForAddBook() {
        Book book = new Book("Harry Potter", "J.K. Rolling", 40.0);
        int size = basket.getBooks().size();
        basket.addBook(book);
        Assertions.assertEquals(size + 1, basket.getBooks().size());
    }

    @Test
    public void testForRemoveBook() {
        basket.clearShoppingCard();
        Assertions.assertEquals(0, basket.getBooks().size());
    }
}
