package AtmPaymentsTests;

import ATMpayments.db.Database;
import ATMpayments.model.Account;
import ATMpayments.model.User;
import ATMpayments.service.ATMbcr;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;

import static org.mockito.Mockito.*;

public class AtmBcrShould {

    @Spy
    static ATMbcr atmBcr;
    User user;

    @Mock
    static Database database;

    @BeforeAll
    static void setUp() {
        atmBcr = spy(ATMbcr.class);
        database = mock(Database.class);
    }

    @BeforeEach
    void setUser(){
        user = dummyUser();
    }

    public static User dummyUser() {
        return new User(new Account("0000", 1000D), "id12345", "DummyUser");
    }

    @Test
    public void makePaymentSuccessfully() {
        Mockito.when(atmBcr.userLog(user)).thenReturn(true);
        atmBcr.payment(user,100D);
        Assertions.assertEquals(user.getAccount().getBalance(),900D);
    }

    @Test
    public void notMakePaymentBecauseNotEnoughMoney(){
        Mockito.when(atmBcr.userLog(user)).thenReturn(true);
        atmBcr.payment(user,2000D);
        Assertions.assertEquals(user.getAccount().getBalance(),1000D);
    }

    @Test
    public void makeDepositSuccessfully(){
        Mockito.when(atmBcr.userLog(user)).thenReturn(true);
        atmBcr.deposit(user,100D);
        Assertions.assertEquals(user.getAccount().getBalance(),1100D);
    }


    @Test
    public void checkPin(){
        Assertions.assertTrue(atmBcr.checkPin(user, "0000"));
    }

    @Test
    public void makeUserLogSuccessfully(){
        atmBcr.setDatabase(database);
        when(database.getUser(user.getId())).thenReturn(user);
        Assertions.assertTrue(atmBcr.userLog(user));
    }

    @Test
    public void notMakeUserLog(){
        atmBcr.setDatabase(database);
        when(database.getUser(user.getId())).thenReturn(null);
        Assertions.assertFalse(atmBcr.userLog(user));
    }
}
