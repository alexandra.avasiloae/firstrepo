import accountsTransfer.Account;
import accountsTransfer.AccountManager;
import com.beust.ah.A;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TransferTests {

    public static Account account1 = new Account(1000.0, "RO234127654312345678987654", "Alexandra Avasiloae");
    public static Account account2 = new Account(2000.50, "RO234127654012345678987654", "Alexandru Boloca");
    public static AccountManager accountManager = new AccountManager();

    @BeforeAll
    static void setUp() {
        accountManager.addAccount(account1);
        accountManager.addAccount(account2);
    }


    @Test
    public void testCorrectAccountNumber() {

        boolean ok = account1.checkAccountNumberForTransfer("RO234127654012345678987654");
        Assertions.assertTrue(ok);
    }

    @Test
    public void testForTooLongAccountNumber() {

        boolean ok = account1.checkAccountNumberForTransfer("RO23412765401234567898765433232");
        Assertions.assertFalse(ok);
    }

    @Test
    public void testForTooShortAccountNumber() {

        boolean ok = account1.checkAccountNumberForTransfer("RO234127");
        Assertions.assertFalse(ok);
    }

    @Test
    public void testAllFieldsCompleted() {
        boolean ok = account1.checkAllFieldForTransfer("RO23412765401234567898765433232", "Alexandra Avasiloae", 70.50);
        Assertions.assertTrue(ok);
    }

    @Test
    public void testFieldsNotCompleted() {
        boolean ok = account1.checkAllFieldForTransfer(null, "Alexandra Avasiloae", 70.50);
        Assertions.assertFalse(ok);
    }

    @Test
    public void testForBalance() {
        String message = account1.transfer("RO234127654012345678987654", "Alexandra Avasiloae", 100070.50);
        Assertions.assertEquals("Insufficient funds", message);
    }

    @Test
    public void testForExistingAccount() {
        String message = account1.transfer("RO234127654012345678987653", "Alexandru Boloca", 70.50);
        Assertions.assertEquals("This account number does not exist!", message);
    }

    @Test
    public void testForTransfer() {
        String message = account1.transfer("RO234127654012345678987654", "Alexandru Boloca", 70.50);
        Assertions.assertEquals("Success", message);
        Assertions.assertEquals(929.50,account1.getBalance());
        Assertions.assertEquals(2071,account2.getBalance());
    }
}
