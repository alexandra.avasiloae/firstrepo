package fileOperations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class FileOperationsTest {

    FileOperations fileOperations  = new FileOperations("myfile.txt");
    @Test
    void createFile() throws IOException {
        Assertions.assertTrue(fileOperations.createFile());
    }

    @Test
    void writeInFile() {
        Assertions.assertTrue(fileOperations.writeInFile("hello"));

    }

    @Test
    void readFromFile() {
        Assertions.assertEquals("hello",fileOperations.readFromFile());
    }
}