import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import usingtTDDapproach.ArrayExample;

import java.util.Arrays;

public class RemoveDuplicatesTests {

    @Test
    public void testRemoveDuplicatesFromAString() {
        String[] duplicates = {"ana", "ana", "maria", "ioana", "alexandra"};

        String[] duplicatesEliminated = ArrayExample.removeDuplicates(duplicates);

        String[] expected = {"ana", "maria", "ioana", "alexandra"};

        Assertions.assertThat(Arrays.toString(duplicatesEliminated)).isEqualTo(Arrays.toString(expected));
    }
}
